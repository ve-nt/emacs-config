;;; Scratch area

;; pdf-view.el needs to be loaded again for the ligther to disappear.
(add-hook 'after-init-hook '(lambda ()
			      (load-file (locate-library "pdf-view.el"))))

;; Fix SML padding issue on Emacs 29
(setq sml/extra-filler -6)



;; Fix corfu's popup on EXWM multi-monitor setups.
(defun corfu--make-frame (frame x y width height)
  "Show current buffer in child frame at X/Y with WIDTH/HEIGHT.
FRAME is the existing frame."
  (when-let (((frame-live-p frame))
             (timer (frame-parameter frame 'corfu--hide-timer)))
    (cancel-timer timer)
    (set-frame-parameter frame 'corfu--hide-timer nil))
  (let* ((window-min-height 1)
         (window-min-width 1)
         (inhibit-redisplay t)
         (x-gtk-resize-child-frames corfu--gtk-resize-child-frames)
         (before-make-frame-hook)
         (after-make-frame-functions)
         (parent (window-frame)))
    (unless (and (frame-live-p frame)
                 (eq (frame-parent frame)
                     (and (not (bound-and-true-p exwm--connection)) parent))
                 ;; If there is more than one window, `frame-root-window' may
                 ;; return nil.  Recreate the frame in this case.
                 (window-live-p (frame-root-window frame)))
      (when frame (delete-frame frame))
      (setq frame (make-frame
                   `((parent-frame . ,parent)
                     (minibuffer . ,(minibuffer-window parent))
                     (width . 0) (height . 0) (visibility . nil)
                     (right-fringe . ,right-fringe-width)
                     (left-fringe . ,left-fringe-width)
                     ,@corfu--frame-parameters))))
    ;; XXX HACK Setting the same frame-parameter/face-background is not a nop.
    ;; Check before applying the setting. Without the check, the frame flickers
    ;; on Mac. We have to apply the face background before adjusting the frame
    ;; parameter, otherwise the border is not updated.
    (let ((new (face-attribute 'corfu-border :background nil 'default)))
      (unless (equal (face-attribute 'internal-border :background frame 'default) new)
        (set-face-background 'internal-border new frame))
      ;; XXX The Emacs Mac Port does not support `internal-border', we also have
      ;; to set `child-frame-border'.
      (unless (or (not (facep 'child-frame-border))
                  (equal (face-attribute 'child-frame-border :background frame 'default) new))
        (set-face-background 'child-frame-border new frame)))
    ;; Reset frame parameters if they changed.  For example `tool-bar-mode'
    ;; overrides the parameter `tool-bar-lines' for every frame, including child
    ;; frames.  The child frame API is a pleasure to work with.  It is full of
    ;; lovely surprises.
    (let* ((win (frame-root-window frame))
           (is (frame-parameters frame))
           (should `((background-color
                      . ,(face-attribute 'corfu-default :background nil 'default))
                     (font . ,(frame-parameter parent 'font))
                     (right-fringe . ,right-fringe-width)
                     (left-fringe . ,left-fringe-width)
                     ,@corfu--frame-parameters))
           (diff (cl-loop for p in should for (k . v) = p
                          unless (equal (alist-get k is) v) collect p)))
      (when diff (modify-frame-parameters frame diff))
      ;; XXX HACK: `set-window-buffer' must be called to force fringe update.
      (when (or diff (eq (window-buffer win) (current-buffer)))
        (set-window-buffer win (current-buffer)))
      ;; Disallow selection of root window (gh:minad/corfu#63)
      (set-window-parameter win 'no-delete-other-windows t)
      (set-window-parameter win 'no-other-window t)
      ;; Mark window as dedicated to prevent frame reuse (gh:minad/corfu#60)
      (set-window-dedicated-p win t))
    (redirect-frame-focus frame parent)
    (set-frame-size frame width height t)
    (unless (equal (frame-position frame) (cons x y))
      (if (bound-and-true-p exwm--connection)
          (set-frame-position
           frame
           (+ x (car (frame-monitor-geometry exwm-workspace--current)))
           (+ y (car (cdr (frame-monitor-geometry exwm-workspace--current)))))
        (set-frame-position frame x y))))
  (make-frame-visible frame)
  ;; Unparent child frame if EXWM is used, otherwise EXWM buffers are drawn on
  ;; top of the Corfu child frame.
  (when (and (bound-and-true-p exwm--connection) (frame-parent frame))
    (set-frame-parameter frame 'parent-frame nil))
  frame)



(defun rephone-replace-all (string to-find to-replace)
  (let ((index  (cl-search to-find string))
        (pos    0)
        (result ""))
    (while index
      (setq result (concat result
                           (substring string pos index)
                           to-replace)
            pos    (+ index (length to-find))
            index  (cl-search to-find string :start2 pos)))
    (concat result (substring string pos))))

(defun rephone-serial-process-filter (process output)
  "Replace LF in output string with CR+LF."
  (term-emulate-terminal process (rephone-replace-all
                                  output
                                  (byte-to-string ?\n)
                                  (string ?\r ?\n))))

(defun rephone-serial-term (port)
  "Basically duplicate SERIAL-TERM from term.el but with process
    filtering to translate LF to CR+LF."
  (interactive (list (serial-read-name)))
  (serial-supported-or-barf)
  (let* ((process (make-serial-process
                   :port port
                   :speed 115200
                   :bytesize 8
                   :parity nil
                   :stopbits 1
                   :flowcontrol nil
                   :coding 'raw-text-unix
                   :noquery t
                   :name (format "Lua:%s" port)
                   :filter 'rephone-serial-process-filter
                   :sentinel 'term-sentinel))
         (buffer (process-buffer process)))
    (with-current-buffer buffer
      (term-mode)
      (term-line-mode)
      (goto-char (point-max))
      (set-marker (process-mark process) (point)))
    (switch-to-buffer buffer)
    buffer))

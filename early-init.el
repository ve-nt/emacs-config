;; This file is loaded automatically, before the package system and GUI
;; is initialized. See "Early Init File" in the Emacs manual (C-h r) for
;; more details.

;; Disable `package'. We use straight instead.
(setq package-enable-at-startup nil)

;; Hack: Change eln-cache location to remove from `user-emacs-directory'
(let ((old-eln-dir (expand-file-name "eln-cache/" user-emacs-directory))
      (new-eln-dir "~/.cache/emacs/eln-cache"))
  (make-directory new-eln-dir t)
  (setq native-comp-eln-load-path
	(remove new-eln-dir (remove old-eln-dir native-comp-eln-load-path)))
  (push new-eln-dir native-comp-eln-load-path))

;; Set auto-save directory
(setq auto-save-list-file-name
      (expand-file-name
       (format "var/auto-save/.saves-%s-%s~"
               (format-time-string "%FT%T") (system-name))
       user-emacs-directory))

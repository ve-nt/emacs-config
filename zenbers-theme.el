;;; zenbers-theme.el --- A fork of zenburn-theme.

;; Author: vent <ventlark@gmail.com>


;; License

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Bozhidar Batsov ported this theme to Emacs and did a really good
;; job. This port is what this package is closely forked off.

;; This theme is intended for my own personal use. I started with
;; Zenburn theme, but have ended up extending it and modifying it over
;; my time using Emacs, and will likely continue to do so in the
;; future.

;; The modifications are made to my own personal taste, and the theme
;; has been extended to support packages that I personally use that
;; were not supported prior.

;; If you want to take this theme and use it for yourself, go right
;; ahead. I have not tested every face modified here, so you may need
;; to do the occasional tinkering here and there.


;; Original commentary:

;; A port of the popular Vim theme Zenbers for Emacs 24+, built on top
;; of the new built-in theme support in Emacs 24.


;;; Credits:

;; Zenburn Author: Bozhidar Batsov <bozhidar@batsov.com>
;; Zenburn URL: http://github.com/bbatsov/zenburn-emacs

;; Original Vim Zenburn Author: Jani Nurminen <slinky@iki.fi>
;; Original Vim Zenburn URL: https://github.com/jnurmine/Zenburn


;;; Code:

(deftheme zenbers "The Zenbers color theme")

(defgroup zenbers-theme nil
  "Zenbers theme."
  :prefix "zenbers-theme-"
  :link '(url-link :tag "GitHub" "http://github.com/bbatsov/zenbers-emacs")
  :tag "Zenbers theme")

(defcustom zenbers-use-variable-pitch nil
  "Use variable pitch face for some headings and titles."
  :type 'boolean
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-height-minus-1 0.8
  "Font size -1."
  :type 'number
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-height-plus-1 1.1
  "Font size +1."
  :type 'number
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-height-plus-2 1.15
  "Font size +2."
  :type 'number
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-height-plus-3 1.2
  "Font size +3."
  :type 'number
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-height-plus-4 1.3
  "Font size +4."
  :type 'number
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-scale-org-headlines nil
  "Whether `org-mode' headlines should be scaled."
  :type 'boolean
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

(defcustom zenbers-scale-outline-headlines nil
  "Whether `outline-mode' headlines should be scaled."
  :type 'boolean
  :group 'zenbers-theme
  :package-version '(zenbers . "2.6"))

;;; Color Palette

(defvar zenbers-default-colors-alist
  '(("zenbers-fg+1"     . "#EEEEEE")
    ("zenbers-fg"       . "#D9D9D9")
    ("zenbers-fg-1"     . "#A8A8A8")
    ("zenbers-fg-2"     . "#888888")
    ("zenbers-bg-2"     . "#000000")
    ("zenbers-bg-1"     . "#2B2B2B")
    ("zenbers-bg-05"    . "#666666")
    ("zenbers-bg"       . "#1C1C1C")
    ("zenbers-bg+05"    . "#1F1F1F")
    ("zenbers-bg+1"     . "#262626")
    ("zenbers-bg+2"     . "#2F2F2F")
    ("zenbers-bg+3"     . "#3A3A3A")
    ("zenbers-red+2"    . "#ECB3B3")
    ("zenbers-red+1"    . "#D7AFAF")
    ("zenbers-red"      . "#D78787")
    ("zenbers-red-1"    . "#BC8383")
    ("zenbers-red-2"    . "#BE7F7F")
    ("zenbers-red-3"    . "#9C6363")
    ("zenbers-red-4"    . "#8C5353")
    ("zenbers-red-5"    . "#7C4343")
    ("zenbers-red-6"    . "#503838")
    ("zenbers-orange"   . "#F5DEC6")
    ("zenbers-yellow"   . "#D7D7AF")
    ("zenbers-yellow-1" . "#E0CF9F")
    ("zenbers-yellow-2" . "#CFC29F")
    ("zenbers-green-5"  . "#2F4F2F")
    ("zenbers-green-4"  . "#3F5F3F")
    ("zenbers-green-3"  . "#4F6F4F")
    ("zenbers-green-2"  . "#5F7F5F")
    ("zenbers-green-1"  . "#6F8F6F")
    ("zenbers-green"    . "#87AF87")
    ("zenbers-green+1"  . "#8FB28F")
    ("zenbers-green+2"  . "#9FC59F")
    ("zenbers-green+3"  . "#AFD8AF")
    ("zenbers-green+4"  . "#BFEBBF")
    ("zenbers-brown"    . "#B7A49C")
    ("zenbers-brown-1"  . "#A48B7C")
    ("zenbers-cyan"     . "#93E0E3")
    ("zenbers-blue+3"   . "#AFD7FF")
    ("zenbers-blue+2"   . "#9FC7EF")
    ("zenbers-blue+1"   . "#9AC0CD")
    ("zenbers-blue"     . "#8CD0D3")
    ("zenbers-blue-1"   . "#7CB8BB")
    ("zenbers-blue-2"   . "#5795CF")
    ("zenbers-blue-3"   . "#5C888B")
    ("zenbers-blue-4"   . "#4C7073")
    ("zenbers-blue-5"   . "#366060")
    ("zenbers-magenta"  . "#BEA7EE"))
  "List of Zenbers colors.
Each element has the form (NAME . HEX).

`+N' suffixes indicate a color is lighter.
`-N' suffixes indicate a color is darker.")

(defmacro zenbers-with-color-variables (&rest body)
  "`let' bind all colors defined in `zenbers-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@(mapcar (lambda (cons)
                     (list (intern (car cons)) (cdr cons)))
                   (append zenbers-default-colors-alist))
         (z-variable-pitch (if zenbers-use-variable-pitch
                               'variable-pitch 'default)))
     ,@body))

;;; Theme Faces
(zenbers-with-color-variables
  (custom-theme-set-faces
   'zenbers
;;;; Built-in
;;;;; basic coloring
   `(button ((t (:foreground ,zenbers-fg-2 :background ,zenbers-bg :underline nil))))
   `(link ((t (:foreground ,zenbers-blue+2 :underline t))))
   `(link-visited ((t (:foreground ,zenbers-magenta :underline t :weight normal))))
   `(default ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(italic ((t (:slant italic))))
   `(cursor ((t (:foreground ,zenbers-fg :background ,zenbers-fg+1))))
   `(widget-field ((t (:foreground ,zenbers-fg :background ,zenbers-bg+1))))
   `(widget-button ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(escape-glyph ((t (:foreground ,zenbers-red+2))))
   `(fringe ((t (:foreground ,zenbers-bg+3 :background ,zenbers-bg))))
   `(header-line ((t (:foreground ,zenbers-green :background ,zenbers-bg :weight bold))))
   `(highlight ((t (:background ,zenbers-bg+1))))
   `(success ((t (:foreground ,zenbers-green :weight bold))))
   `(warning ((t (:foreground ,zenbers-orange :weight bold))))
   `(shadow ((t (:foreground ,zenbers-bg-05))))
   `(error ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(tooltip ((t (:foreground ,zenbers-fg :background ,zenbers-bg+1))))
   `(underline ((t (:foreground ,zenbers-fg :background ,zenbers-bg :underline nil))))
   `(variable-pitch ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(custom-button ((t (:foreground ,zenbers-fg :background ,zenbers-bg+2))))
   `(custom-button-unraised ((t (:foreground ,zenbers-fg :background ,zenbers-bg+2))))
   `(fixed-pitch ((t)))
   `(mouse ((t (:foreground ,zenbers-fg-2 :background ,zenbers-bg))))
;;;;; Customize
   `(custom-group-tag ((t (:foreground ,zenbers-orange :background ,zenbers-bg :weight bold))))
   `(custom-face-tag ((t (:foreground ,zenbers-green+1 :background ,zenbers-bg :weight bold))))
   `(custom-variable-tag ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg))))
;;;;; compilation
   `(compilation-column-face ((t (:foreground ,zenbers-yellow))))
   `(compilation-enter-directory-face ((t (:foreground ,zenbers-green))))
   `(compilation-error-face ((t (:foreground ,zenbers-red-1 :weight bold :underline t))))
   `(compilation-face ((t (:foreground ,zenbers-fg))))
   `(compilation-info-face ((t (:foreground ,zenbers-blue))))
   `(compilation-info ((t (:foreground ,zenbers-green+1 :underline nil))))
   `(compilation-leave-directory-face ((t (:foreground ,zenbers-green))))
   `(compilation-line-face ((t (:foreground ,zenbers-fg-1 :underline nil))))
   `(compilation-line-number ((t (:foreground ,zenbers-fg-1 :underline nil))))
   `(compilation-message-face ((t (:foreground ,zenbers-blue))))
   `(compilation-warning-face ((t (:foreground ,zenbers-orange :weight bold :underline t))))
   `(compilation-mode-line-exit ((t (:foreground ,zenbers-green+2 :weight bold))))
   `(compilation-mode-line-fail ((t (:foreground ,zenbers-red :weight bold))))
   `(compilation-mode-line-run ((t (:foreground ,zenbers-yellow :weight bold))))
;;;;; completions
   `(completions-annotations ((t (:inherit ,font-lock-type-face))))
   `(completions-common-part ((t (:foreground ,zenbers-fg-1))))
   `(completions-first-difference ((t (:foreground ,zenbers-fg))))
;;;;; completion-preview
   `(completion-preview ((t (:foreground ,zenbers-fg-2))))
   `(completion-preview-exact ((t (:foreground ,zenbers-fg-2 :underline t))))
;;;;; eww
   '(eww-invalid-certificate ((t (:inherit error))))
   '(eww-valid-certificate   ((t (:inherit success))))
   `(eww-form-text ((t (:box nil :foreground ,zenbers-fg :background ,zenbers-bg+2))))
   `(eww-form-checkbox ((t (:background ,zenbers-bg+2 :foreground ,zenbers-fg))))
   `(eww-form-submit ((t (:background ,zenbers-bg+3 :foreground ,zenbers-fg))))
;;;;; eglot
   `(eglot-mode-line ((t (:foreground ,zenbers-fg-1 :weight bold))))
;;;;; grep
   `(grep-context-face ((t (:foreground ,zenbers-fg))))
   `(grep-error-face ((t (:foreground ,zenbers-red-1 :weight bold :underline t))))
   `(grep-hit-face ((t (:foreground ,zenbers-blue))))
   `(grep-match-face ((t (:foreground ,zenbers-magenta :weight bold))))
   `(match ((t (:foreground ,zenbers-magenta :weight bold))))
;;;;; hi-lock
   `(hi-blue    ((t (:background ,zenbers-blue+1  :foreground ,zenbers-bg-1))))
   `(hi-green   ((t (:background ,zenbers-green+4 :foreground ,zenbers-bg-1))))
   `(hi-pink    ((t (:background ,zenbers-magenta :foreground ,zenbers-bg-1))))
   `(hi-yellow  ((t (:background ,zenbers-yellow  :foreground ,zenbers-bg-1))))
   `(hi-blue-b  ((t (:foreground ,zenbers-blue+1  :weight     bold))))
   `(hi-green-b ((t (:foreground ,zenbers-green+2 :weight     bold))))
   `(hi-red-b   ((t (:foreground ,zenbers-red     :weight     bold))))
;;;;; info
   `(Info-quoted ((t (:inherit font-lock-constant-face))))
;;;;; isearch
   `(isearch ((t (:inherit match))))
   `(isearch-fail ((t (:foreground ,zenbers-fg :background ,zenbers-red-4))))
   `(lazy-highlight ((t (:foreground ,zenbers-magenta))))

   `(menu ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(minibuffer-prompt ((t (:foreground ,zenbers-blue+3))))
   `(help-key-binding ((t (:foreground ,zenbers-blue+3))))
   `(mode-line ((,class (:foreground ,zenbers-fg-1 :background ,zenbers-bg+3)) (t :inverse-video t)))
   `(mode-line-inactive ((t (:foreground ,zenbers-bg-05 :background ,zenbers-bg+1))))
   `(mode-line-highlight ((t (:background ,zenbers-bg+3))))
   `(region ((,class (:background ,zenbers-bg+3))
             (t :inverse-video t)))
   `(secondary-selection ((t (:background ,zenbers-bg+2))))
   `(trailing-whitespace ((t (:background ,zenbers-red))))
   `(vertical-border ((t (:foreground ,zenbers-bg+3))))
;;;;; font lock
   `(font-lock-builtin-face ((t (:foreground ,zenbers-fg-1))))
   `(font-lock-comment-face ((t (:foreground ,zenbers-green))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,zenbers-green-2))))
   `(font-lock-constant-face ((t (:foreground ,zenbers-brown))))
   `(font-lock-doc-face ((t (:foreground ,zenbers-green+2))))
   `(font-lock-function-name-face ((t (:foreground ,zenbers-fg-1))))
   `(font-lock-function ((t (:foreground ,zenbers-red+1))))
   `(font-lock-keyword-face ((t (:foreground ,zenbers-orange :weight bold))))
   `(font-lock-negation-char-face ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(font-lock-preprocessor-face ((t (:foreground ,zenbers-fg-1))))
   `(font-lock-regexp-grouping-construct ((t (:foreground ,zenbers-yellow :weight bold))))
   `(font-lock-regexp-grouping-backslash ((t (:foreground ,zenbers-green :weight bold))))
   `(font-lock-string-face ((t (:foreground ,zenbers-red))))
   `(font-lock-type-face ((t (:foreground ,zenbers-red+1))))
   `(font-lock-variable-name-face ((t (:foreground ,zenbers-fg :underline nil))))
   `(font-lock-warning-face ((t (:foreground ,zenbers-yellow-2 :weight bold))))

   `(highlight-numbers-number ((t (:foreground ,zenbers-cyan))))
   `(highlight-function-calls-face ((t (:foreground ,zenbers-fg-1))))
   `(c-annotation-face ((t (:inherit font-lock-constant-face))))
   `(hes-escape-sequence-face ((t (:foreground ,zenbers-red+2))))
   `(hes-escape-backslash-face ((t (:foreground ,zenbers-red+2))))

;;;;; Doxygen highlighting
   `(highlight-doxygen-comment ((t (:foreground ,zenbers-green+2))))

;;;;; Rust
   `(rust-builtin-formatting-macro-face ((t (:foreground ,zenbers-fg-1))))
   `(rustic-errno-face ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(rustic-unsafe-face ((t (:foreground ,zenbers-red+2 :weight bold))))
   `(rustic-popup-section-face ((t (:foreground ,zenbers-green :weight bold))))
   `(rustic-popup-key-face ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(rustic-compilation-info-face ((t (:foreground ,zenbers-green :weight bold))))
   `(rustic-cargo-outdated-face ((t (:foreground ,zenbers-red+1))))
   `(rustic-cargo-outdated-upgrade-face ((t (:foreground ,zenbers-blue+1))))
   `(rust-builtin-formatting-macro-face ((t (:foreground ,zenbers-fg-2))))
;;;;; line numbers (Emacs 26.1 and above)
   `(line-number ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg+2))))
   `(line-number-current-line ((t (:inherit line-number :background ,zenbers-bg+2 :foreground ,zenbers-fg-1 :weight bold))))
;;;;; man
   '(Man-overstrike ((t (:inherit font-lock-keyword-face))))
   '(Man-underline  ((t (:inherit (font-lock-string-face underline)))))
;;;;; newsticker
   `(newsticker-date-face ((t (:foreground ,zenbers-fg))))
   `(newsticker-default-face ((t (:foreground ,zenbers-fg))))
   `(newsticker-enclosure-face ((t (:foreground ,zenbers-green+3))))
   `(newsticker-extra-face ((t (:foreground ,zenbers-bg+2 :height 0.8))))
   `(newsticker-feed-face ((t (:foreground ,zenbers-fg))))
   `(newsticker-immortal-item-face ((t (:foreground ,zenbers-green))))
   `(newsticker-new-item-face ((t (:foreground ,zenbers-blue))))
   `(newsticker-obsolete-item-face ((t (:foreground ,zenbers-red))))
   `(newsticker-old-item-face ((t (:foreground ,zenbers-bg+3))))
   `(newsticker-statistics-face ((t (:foreground ,zenbers-fg))))
   `(newsticker-treeview-face ((t (:foreground ,zenbers-fg))))
   `(newsticker-treeview-immortal-face ((t (:foreground ,zenbers-green))))
   `(newsticker-treeview-listwindow-face ((t (:foreground ,zenbers-fg))))
   `(newsticker-treeview-new-face ((t (:foreground ,zenbers-blue :weight bold))))
   `(newsticker-treeview-obsolete-face ((t (:foreground ,zenbers-red))))
   `(newsticker-treeview-old-face ((t (:foreground ,zenbers-bg+3))))
   `(newsticker-treeview-selection-face ((t (:background ,zenbers-bg-1 :foreground ,zenbers-yellow))))
;;;;; woman
   '(woman-bold   ((t (:inherit font-lock-keyword-face))))
   '(woman-italic ((t (:inherit (font-lock-string-face italic)))))
;;;; Third-party
;;;;; ace-jump
   `(ace-jump-face-background
     ((t (:foreground ,zenbers-bg+3 :background ,zenbers-bg :inverse-video nil))))
   `(ace-jump-face-foreground
     ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg :inverse-video nil))))
;;;;; ace-window
   `(aw-background-face ((t (:inherit ace-jump-face-background))))
   `(aw-leading-char-face ((t (:inherit ace-jump-face-foreground))))
;;;;; android mode
   `(android-mode-debug-face ((t (:foreground ,zenbers-green+1))))
   `(android-mode-error-face ((t (:foreground ,zenbers-orange :weight bold))))
   `(android-mode-info-face ((t (:foreground ,zenbers-fg))))
   `(android-mode-verbose-face ((t (:foreground ,zenbers-green))))
   `(android-mode-warning-face ((t (:foreground ,zenbers-yellow))))
;;;;; anzu
   `(anzu-mode-line ((t (:foreground ,zenbers-cyan :weight bold))))
   `(anzu-mode-line-no-match ((t (:foreground ,zenbers-red :weight bold))))
   `(anzu-match-1 ((t (:foreground ,zenbers-bg :background ,zenbers-green))))
   `(anzu-match-2 ((t (:foreground ,zenbers-bg :background ,zenbers-orange))))
   `(anzu-match-3 ((t (:foreground ,zenbers-bg :background ,zenbers-blue))))
   `(anzu-replace-to ((t (:inherit anzu-replace-highlight :foreground ,zenbers-yellow))))
;;;;; auctex
   `(font-latex-bold-face ((t (:inherit bold))))
   `(font-latex-warning-face ((t (:foreground nil :inherit font-lock-warning-face))))
   `(font-latex-sectioning-5-face ((t (:foreground ,zenbers-red :weight bold ))))
   `(font-latex-sedate-face ((t (:foreground ,zenbers-red+1))))
   `(font-latex-italic-face ((t (:foreground ,zenbers-fg-1 :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,zenbers-orange))))
   `(font-latex-script-char-face ((t (:foreground ,zenbers-orange))))
   `(font-latex-verbatim-face ((t (:foreground ,zenbers-red))))
;;;;; agda-mode
   `(agda2-highlight-keyword-face ((t (:foreground ,zenbers-yellow :weight bold))))
   `(agda2-highlight-string-face ((t (:foreground ,zenbers-red))))
   `(agda2-highlight-symbol-face ((t (:foreground ,zenbers-orange))))
   `(agda2-highlight-primitive-type-face ((t (:foreground ,zenbers-blue-1))))
   `(agda2-highlight-inductive-constructor-face ((t (:foreground ,zenbers-fg))))
   `(agda2-highlight-coinductive-constructor-face ((t (:foreground ,zenbers-fg))))
   `(agda2-highlight-datatype-face ((t (:foreground ,zenbers-blue))))
   `(agda2-highlight-function-face ((t (:foreground ,zenbers-blue))))
   `(agda2-highlight-module-face ((t (:foreground ,zenbers-blue-1))))
   `(agda2-highlight-error-face ((t (:foreground ,zenbers-bg :background ,zenbers-magenta))))
   `(agda2-highlight-unsolved-meta-face ((t (:foreground ,zenbers-bg :background ,zenbers-magenta))))
   `(agda2-highlight-unsolved-constraint-face ((t (:foreground ,zenbers-bg :background ,zenbers-magenta))))
   `(agda2-highlight-termination-problem-face ((t (:foreground ,zenbers-bg :background ,zenbers-magenta))))
   `(agda2-highlight-incomplete-pattern-face ((t (:foreground ,zenbers-bg :background ,zenbers-magenta))))
   `(agda2-highlight-typechecks-face ((t (:background ,zenbers-red-4))))
;;;;; auto-complete
   `(ac-candidate-face ((t (:background ,zenbers-bg+1 :foreground ,zenbers-fg))))
   `(ac-selection-face ((t (:background ,zenbers-bg+3 :foreground ,zenbers-fg))))
   `(popup-tip-face ((t (:background ,zenbers-yellow-2 :foreground ,zenbers-bg-2))))
   `(popup-menu-mouse-face ((t (:background ,zenbers-yellow-2 :foreground ,zenbers-bg-2))))
   `(popup-summary-face ((t (:background ,zenbers-bg+3 :foreground ,zenbers-bg-2))))
   `(popup-scroll-bar-foreground-face ((t (:background ,zenbers-blue-5))))
   `(popup-scroll-bar-background-face ((t (:background ,zenbers-bg-1))))
   `(popup-isearch-match ((t (:background ,zenbers-bg :foreground ,zenbers-fg))))
;;;;; avy
   `(avy-background-face
     ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg :inverse-video nil))))
   `(avy-lead-face-0
     ((t (:foreground ,zenbers-green+3 :background ,zenbers-bg :inverse-video nil :weight bold))))
   `(avy-lead-face-1
     ((t (:foreground ,zenbers-yellow :background ,zenbers-bg :inverse-video nil :weight bold))))
   `(avy-lead-face-2
     ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg :inverse-video nil :weight bold))))
   `(avy-lead-face
     ((t (:foreground ,zenbers-cyan :background ,zenbers-bg :inverse-video nil :weight bold))))
;;;;; company-mode
   `(company-tooltip ((t (:foreground ,zenbers-fg :background ,zenbers-bg+1))))
   `(company-tooltip-annotation ((t (:foreground ,zenbers-orange :background ,zenbers-bg+1))))
   `(company-tooltip-annotation-selection ((t (:foreground ,zenbers-orange :background ,zenbers-bg+3))))
   `(company-tooltip-selection ((t (:foreground ,zenbers-fg :background ,zenbers-bg+3))))
   `(company-tooltip-mouse ((t (:background ,zenbers-bg-1))))
   `(company-tooltip-common ((t (:foreground ,zenbers-green+2))))
   `(company-tooltip-common-selection ((t (:foreground ,zenbers-green+2))))
   `(company-scrollbar-fg ((t (:background ,zenbers-bg+3))))
   `(company-scrollbar-bg ((t (:background ,zenbers-bg+1))))
   `(company-preview ((t (:foreground ,zenbers-fg :background ,zenbers-bg+3))))
   `(company-preview-common ((t (:foreground ,zenbers-fg :background ,zenbers-bg+3))))
   `(company-template-field ((t (:foreground ,zenbers-fg :background ,zenbers-bg+2))))
;;;;; corfu
   `(corfu-default ((t (:foreground ,zenbers-fg :background ,zenbers-bg+05))))
   `(corfu-current ((t (:background ,zenbers-bg+2))))
   `(corfu-annotations ((t (:inherit ,font-lock-type-face))))
   `(corfu-deprecated ((t (:inherit ,font-lock-type-face))))
   `(corfu-popupinfo ((t (:inherit ,font-lock-type-face))))
   `(corfu-echo ((t (:inherit ,font-lock-type-face))))
   `(corfu-border ((t (:background ,zenbers-fg-2))))
;;;;; bm
   `(bm-face ((t (:background ,zenbers-yellow-1 :foreground ,zenbers-bg))))
   `(bm-fringe-face ((t (:background ,zenbers-yellow-1 :foreground ,zenbers-bg))))
   `(bm-fringe-persistent-face ((t (:background ,zenbers-green-2 :foreground ,zenbers-bg))))
   `(bm-persistent-face ((t (:background ,zenbers-green-2 :foreground ,zenbers-bg))))
;;;;; calfw
   `(cfw:face-annotation ((t (:foreground ,zenbers-red :inherit cfw:face-day-title))))
   `(cfw:face-day-title ((t nil)))
   `(cfw:face-default-content ((t (:foreground ,zenbers-green))))
   `(cfw:face-default-day ((t (:weight bold))))
   `(cfw:face-disable ((t (:foreground ,zenbers-fg-1))))
   `(cfw:face-grid ((t (:inherit shadow))))
   `(cfw:face-header ((t (:inherit font-lock-keyword-face))))
   `(cfw:face-holiday ((t (:inherit cfw:face-sunday))))
   `(cfw:face-periods ((t (:foreground ,zenbers-cyan))))
   `(cfw:face-saturday ((t (:foreground ,zenbers-blue :weight bold))))
   `(cfw:face-select ((t (:background ,zenbers-blue-5))))
   `(cfw:face-sunday ((t (:foreground ,zenbers-red :weight bold))))
   `(cfw:face-title ((t (:height 2.0 :inherit (variable-pitch font-lock-keyword-face)))))
   `(cfw:face-today ((t (:foreground ,zenbers-cyan :weight bold))))
   `(cfw:face-today-title ((t (:inherit highlight bold))))
   `(cfw:face-toolbar ((t (:background ,zenbers-blue-5))))
   `(cfw:face-toolbar-button-off ((t (:underline nil :inherit link))))
   `(cfw:face-toolbar-button-on ((t (:underline nil :inherit link-visited))))
;;;;; cider
   `(cider-result-overlay-face ((t (:background unspecified))))
   `(cider-enlightened-face ((t (:box (:color ,zenbers-orange :line-width -1)))))
   `(cider-enlightened-local-face ((t (:weight bold :foreground ,zenbers-green+1))))
   `(cider-deprecated-face ((t (:background ,zenbers-yellow-2))))
   `(cider-instrumented-face ((t (:box (:color ,zenbers-red :line-width -1)))))
   `(cider-traced-face ((t (:box (:color ,zenbers-cyan :line-width -1)))))
   `(cider-test-failure-face ((t (:background ,zenbers-red-4))))
   `(cider-test-error-face ((t (:background ,zenbers-magenta))))
   `(cider-test-success-face ((t (:background ,zenbers-green-2))))
   `(cider-fringe-good-face ((t (:foreground ,zenbers-green+4))))
;;;;; circe
   `(circe-highlight-nick-face ((t (:foreground ,zenbers-cyan))))
   `(circe-my-message-face ((t (:foreground ,zenbers-fg))))
   `(circe-fool-face ((t (:foreground ,zenbers-red+1))))
   `(circe-topic-diff-removed-face ((t (:foreground ,zenbers-red :weight bold))))
   `(circe-originator-face ((t (:foreground ,zenbers-fg))))
   `(circe-server-face ((t (:foreground ,zenbers-green))))
   `(circe-topic-diff-new-face ((t (:foreground ,zenbers-orange :weight bold))))
   `(circe-prompt-face ((t (:foreground ,zenbers-orange :background ,zenbers-bg :weight bold))))
;;;;; consult
   `(consult-imenu-prefix ((t :inherit ,font-lock-type-face)))
   `(consult-line-number-prefix ((t :foreground ,zenbers-fg :background ,zenbers-bg+1)))
   `(consult-line-number-wrapped ((t :foreground ,zenbers-fg-2 :background ,zenbers-bg+1)))
   `(consult-key ((t :inherit marginalia-key)))
   `(consult-file ((t :inherit marginalia-file)))
;;;;; context-coloring
   `(context-coloring-level-0-face ((t :foreground ,zenbers-fg)))
   `(context-coloring-level-1-face ((t :foreground ,zenbers-cyan)))
   `(context-coloring-level-2-face ((t :foreground ,zenbers-green+4)))
   `(context-coloring-level-3-face ((t :foreground ,zenbers-yellow)))
   `(context-coloring-level-4-face ((t :foreground ,zenbers-orange)))
   `(context-coloring-level-5-face ((t :foreground ,zenbers-magenta)))
   `(context-coloring-level-6-face ((t :foreground ,zenbers-blue+1)))
   `(context-coloring-level-7-face ((t :foreground ,zenbers-green+2)))
   `(context-coloring-level-8-face ((t :foreground ,zenbers-yellow-2)))
   `(context-coloring-level-9-face ((t :foreground ,zenbers-red+1)))
;;;;; coq
   `(coq-solve-tactics-face ((t (:foreground nil :inherit font-lock-constant-face))))
;;;;; crontab
   `(crontab-minute ((t (:inherit org-level-1))))
   `(crontab-hour ((t (:inherit org-level-2))))
   `(crontab-month-day ((t (:inherit org-level-3))))
   `(crontab-month ((t (:inherit org-level-4))))
   `(crontab-week-day ((t (:inherit org-level-5))))
;;;;; ctable
   `(ctbl:face-cell-select ((t (:background ,zenbers-blue :foreground ,zenbers-bg))))
   `(ctbl:face-continue-bar ((t (:background ,zenbers-bg-05 :foreground ,zenbers-bg))))
   `(ctbl:face-row-select ((t (:background ,zenbers-cyan :foreground ,zenbers-bg))))
;;;;; dashboard
   `(dashboard-banner-logo-title ((t (:foreground ,zenbers-bg))))
   `(dashboard-text-banner ((t (:foreground ,zenbers-fg-2))))
   `(dashboard-heading-face ((t (:foreground ,zenbers-green :weight bold))))
   `(dashboard-text-banner ((t (:foreground ,zenbers-green :weight bold))))
;;;;; debbugs
   `(debbugs-gnu-done ((t (:foreground ,zenbers-fg-1))))
   `(debbugs-gnu-handled ((t (:foreground ,zenbers-green))))
   `(debbugs-gnu-new ((t (:foreground ,zenbers-red))))
   `(debbugs-gnu-pending ((t (:foreground ,zenbers-blue))))
   `(debbugs-gnu-stale ((t (:foreground ,zenbers-orange))))
   `(debbugs-gnu-tagged ((t (:foreground ,zenbers-red))))
;;;;; diff
   `(diff-added          ((t (:foreground ,zenbers-green+2))))
   `(diff-changed        ((t (:foreground ,zenbers-yellow-1))))
   `(diff-removed        ((t (:foreground ,zenbers-red+2))))
   `(diff-refine-added   ((t (:background ,zenbers-bg+1 :foreground ,zenbers-green+3))))
   `(diff-refine-changed ((t (:background ,zenbers-bg+1 :foreground ,zenbers-yellow))))
   `(diff-refine-removed ((t (:background ,zenbers-bg+1 :foreground ,zenbers-red+2))))
   `(diff-header ((,class (:background ,zenbers-bg+2))
                  (t (:background ,zenbers-fg :foreground ,zenbers-bg))))
   `(diff-indicator-added ((t (:inherit diff-added))))
   `(diff-indicator-changed ((t (:inherit diff-changed))))
   `(diff-indicator-removed ((t (:inherit diff-removed))))
   `(diff-file-header
     ((,class (:background ,zenbers-bg+2 :foreground ,zenbers-fg :weight bold))
      (t (:background ,zenbers-fg :foreground ,zenbers-bg :weight bold))))
;;;;; diff-hl
   `(diff-hl-change ((,class (:foreground ,zenbers-blue :background ,zenbers-blue-2))))
   `(diff-hl-delete ((,class (:foreground ,zenbers-red+1 :background ,zenbers-red-1))))
   `(diff-hl-insert ((,class (:foreground ,zenbers-green+1 :background ,zenbers-green-2))))
;;;;; dim-autoload
   `(dim-autoload-cookie-line ((t :foreground ,zenbers-bg+1)))
;;;;; dired
   `(dired-directory ((t (:foreground ,zenbers-green :weight bold))))
   `(dired-symlink ((t (:foreground ,zenbers-blue+2))))
;;;;; dired+
   `(diredp-display-msg ((t (:foreground ,zenbers-blue))))
   `(diredp-compressed-file-suffix ((t (:foreground ,zenbers-red+2))))
   `(diredp-date-time ((t (:foreground ,zenbers-fg-2))))
   `(diredp-deletion ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(diredp-deletion-file-name ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(diredp-dir-name ((t (:foreground ,zenbers-green :background ,zenbers-bg :weight bold))))
   `(diredp-dir-heading ((t (:foreground ,zenbers-blue+2 :background ,zenbers-bg))))
   `(diredp-dir-priv ((t (:foreground ,zenbers-fg-1 :weight bold))))
   `(diredp-executable-tag ((t (:foreground ,zenbers-green+1))))
   `(diredp-file-name ((t (:foreground ,zenbers-fg))))
   `(diredp-omit-file-name ((t (:foreground ,zenbers-fg-2))))
   `(diredp-file-suffix ((t (:foreground ,zenbers-fg-2))))
   `(diredp-flag-mark ((t (:foreground ,zenbers-blue+2 :weight bold))))
   `(diredp-flag-mark-line ((t (:foreground ,zenbers-blue+2 :weight bold))))
   `(diredp-ignored-file-name ((t (:foreground ,zenbers-red))))
   `(diredp-link-priv ((t (:foreground ,zenbers-fg-2))))
   `(diredp-mode-line-flagged ((t (:foreground ,zenbers-red))))
   `(diredp-mode-line-marked ((t (:foreground ,zenbers-orange :weight bold))))
   `(diredp-no-priv ((t (:foreground ,zenbers-fg-2))))
   `(diredp-number ((t (:foreground ,zenbers-fg-2))))
   `(diredp-other-priv ((t (:foreground ,zenbers-fg-2))))
   `(diredp-rare-priv ((t (:foreground ,zenbers-red-1))))
   `(diredp-symlink ((t (:inherit dired-symlink))))
   `(diredp-read-priv ((t (:foreground ,zenbers-red+2))))
   `(diredp-write-priv ((t (:foreground ,zenbers-blue+2))))
   `(diredp-exec-priv ((t (:foreground ,zenbers-green+2))))
;;;;; dired-async
   `(dired-async-failures ((t (:foreground ,zenbers-red :weight bold))))
   `(dired-async-message ((t (:foreground ,zenbers-yellow :weight bold))))
   `(dired-async-mode-message ((t (:foreground ,zenbers-yellow))))
;;;;; diredfl
   `(diredfl-compressed-file-suffix ((t (:foreground ,zenbers-orange))))
   `(diredfl-date-time ((t (:foreground ,zenbers-magenta))))
   `(diredfl-deletion ((t (:foreground ,zenbers-yellow))))
   `(diredfl-deletion-file-name ((t (:foreground ,zenbers-red))))
   `(diredfl-dir-heading ((t (:foreground ,zenbers-blue :background ,zenbers-bg-1))))
   `(diredfl-dir-priv ((t (:foreground ,zenbers-cyan))))
   `(diredfl-exec-priv ((t (:foreground ,zenbers-red))))
   `(diredfl-executable-tag ((t (:foreground ,zenbers-green+1))))
   `(diredfl-file-name ((t (:foreground ,zenbers-blue))))
   `(diredfl-file-suffix ((t (:foreground ,zenbers-green))))
   `(diredfl-flag-mark ((t (:foreground ,zenbers-yellow))))
   `(diredfl-flag-mark-line ((t (:foreground ,zenbers-orange))))
   `(diredfl-ignored-file-name ((t (:foreground ,zenbers-red))))
   `(diredfl-link-priv ((t (:foreground ,zenbers-yellow))))
   `(diredfl-no-priv ((t (:foreground ,zenbers-fg))))
   `(diredfl-number ((t (:foreground ,zenbers-green+1))))
   `(diredfl-other-priv ((t (:foreground ,zenbers-yellow-1))))
   `(diredfl-rare-priv ((t (:foreground ,zenbers-red-1))))
   `(diredfl-read-priv ((t (:foreground ,zenbers-green-1))))
   `(diredfl-symlink ((t (:foreground ,zenbers-yellow))))
   `(diredfl-write-priv ((t (:foreground ,zenbers-magenta))))
;;;;; dired-subtree
   `(dired-subtree-depth-1-face ((nil)))
   `(dired-subtree-depth-2-face ((nil)))
   `(dired-subtree-depth-3-face ((nil)))
   `(dired-subtree-depth-4-face ((nil)))
   `(dired-subtree-depth-5-face ((nil)))
   `(dired-subtree-depth-6-face ((nil)))
;;;;; ediff
   `(ediff-current-diff-A ((t (:background ,zenbers-bg+1 :inherit diff-removed))))
   `(ediff-current-diff-Ancestor ((t (:inherit ediff-current-diff-A))))
   `(ediff-current-diff-B ((t (:background ,zenbers-bg+1 :inherit diff-added))))
   `(ediff-current-diff-C ((t (:background ,zenbers-bg+1 :foreground ,zenbers-blue+1))))
   `(ediff-even-diff-Ancestor ((t (:background ,zenbers-bg+1))))
   `(ediff-even-diff-A ((t (:background ,zenbers-bg+05))))
   `(ediff-even-diff-B ((t (:background ,zenbers-bg+05))))
   `(ediff-even-diff-C ((t (:background ,zenbers-bg+05))))
   `(ediff-fine-diff-Ancestor ((t (:inherit ediff-fine-diff-A))))
   `(ediff-fine-diff-A ((t (:background ,zenbers-bg+1 :inherit diff-refine-removed :weight bold))))
   `(ediff-fine-diff-B ((t (:background ,zenbers-bg+1 :inherit diff-refine-added :weight bold))))
   `(ediff-fine-diff-C ((t (:background ,zenbers-bg+1 :foreground ,zenbers-blue+3 :weight bold))))
   `(ediff-odd-diff-Ancestor ((t (:inherit ediff-odd-diff-A))))
   `(ediff-odd-diff-A ((t (:background ,zenbers-bg+05))))
   `(ediff-odd-diff-B ((t (:background ,zenbers-bg+05 :inherit ediff-odd-diff-A))))
   `(ediff-odd-diff-C ((t (:inherit ediff-odd-diff-A))))
;;;;; egg
   `(egg-text-base ((t (:foreground ,zenbers-fg))))
   `(egg-help-header-1 ((t (:foreground ,zenbers-yellow))))
   `(egg-help-header-2 ((t (:foreground ,zenbers-green+3))))
   `(egg-branch ((t (:foreground ,zenbers-yellow))))
   `(egg-branch-mono ((t (:foreground ,zenbers-yellow))))
   `(egg-term ((t (:foreground ,zenbers-yellow))))
   `(egg-diff-add ((t (:foreground ,zenbers-green+4))))
   `(egg-diff-del ((t (:foreground ,zenbers-red+1))))
   `(egg-diff-file-header ((t (:foreground ,zenbers-yellow-2))))
   `(egg-section-title ((t (:foreground ,zenbers-yellow))))
   `(egg-stash-mono ((t (:foreground ,zenbers-green+4))))
;;;;; elfeed
   `(elfeed-log-error-level-face ((t (:foreground ,zenbers-red))))
   `(elfeed-log-info-level-face ((t (:foreground ,zenbers-blue))))
   `(elfeed-log-warn-level-face ((t (:foreground ,zenbers-yellow))))
   `(elfeed-search-title-face ((t (:foreground ,zenbers-fg-2))))
   `(elfeed-search-unread-title-face ((t (:foreground ,zenbers-fg))))
   `(elfeed-search-date-face ((t (:foreground ,zenbers-green))))
   `(elfeed-search-tag-face ((t (:foreground ,zenbers-fg-2))))
   `(elfeed-search-feed-face ((t (:foreground ,zenbers-red))))
;;;;; emacs-w3m
   `(w3m-anchor ((t (:foreground ,zenbers-blue+1 :weight bold))))
   `(w3m-arrived-anchor ((t (:foreground ,zenbers-magenta :weight normal))))
   `(w3m-image ((t (:foreground ,zenbers-green+3 :weight normal))))
   `(w3m-image-anchor ((t (:foreground ,zenbers-green :weight normal))))
   `(w3m-form ((t (:foreground ,zenbers-red-1 :box t :underline nil :style nil))))
   `(w3m-current-anchor ((t (:style nil))))
   `(w3m-header-line-location-title ((t (:foreground ,zenbers-yellow :underline t :weight bold))))
   '(w3m-history-current-url ((t (:inherit match))))
   `(w3m-lnum ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
   `(w3m-lnum-match ((t (:background ,zenbers-bg-1
                                     :foreground ,zenbers-orange
                                     :weight bold))))
   `(w3m-lnum-minibuffer-prompt ((t (:foreground ,zenbers-yellow))))
   `(w3m-form-button ((t (:foreground ,zenbers-orange :weight bold))))
   `(w3m-form-button-mouse ((t (:foreground ,zenbers-orange :weight bold))))
   `(w3m-form-button-pressed ((t (:foreground ,zenbers-orange :weight bold :box t))))
;;;;; emms
   `(emms-browser-album-face ((t (:foreground ,zenbers-green))))
   `(emms-browser-artist-face ((t (:foreground ,zenbers-green))))
   `(emms-browser-composer-face ((t (:foreground ,zenbers-green))))
   `(emms-browser-performer-face ((t (:foreground ,zenbers-green))))
   `(emms-browser-track-face ((t (:foreground ,zenbers-red))))
   `(emms-browser-year/genre-face ((t (:foreground ,zenbers-fg-2))))
   `(emms-playlist-selected-face ((t (:foreground ,zenbers-red+2))))
   `(emms-playlist-track-face ((t (:foreground ,zenbers-red))))
   `(helm-emms-playlist ((t (:foreground ,zenbers-fg))))
;;;;; erc
   `(erc-action-face ((t (:foreground ,zenbers-magenta))))
   `(erc-bold-face ((t (:weight bold))))
   `(erc-current-nick-face ((t (:foreground ,zenbers-blue))))
   `(erc-dangerous-host-face ((t (:inherit font-lock-warning-face))))
   `(erc-default-face ((t (:foreground ,zenbers-fg))))
   `(erc-direct-msg-face ((t (:inherit erc-default-face))))
   `(erc-error-face ((t (:inherit font-lock-warning-face))))
   `(erc-fool-face ((t (:inherit erc-default-face))))
   `(erc-highlight-face ((t (:inherit hover-highlight))))
   `(erc-input-face ((t (:foreground ,zenbers-fg))))
   `(erc-keyword-face ((t (:foreground ,zenbers-blue :weight bold))))
   `(erc-nick-default-face ((t (:foreground ,zenbers-fg))))
   `(erc-my-nick-face ((t (:foreground ,zenbers-red))))
   `(erc-nick-msg-face ((t (:inherit erc-default-face))))
   `(erc-notice-face ((t (:foreground ,zenbers-green))))
   `(erc-pal-face ((t (:foreground ,zenbers-orange :weight bold))))
   `(erc-prompt-face ((t (:foreground ,zenbers-blue+3 :background ,zenbers-bg))))
   `(erc-header-line ((t (:foreground ,zenbers-fg-2 :background ,zenbers-bg :underline nil))))
   `(erc-timestamp-face ((t (:foreground ,zenbers-green))))
   `(erc-underline-face ((t (:underline t))))
   `(erc-nick-prefix-face ((t (:foreground ,zenbers-fg-2 :weight bold))))
   `(erc-my-nick-prefix-face ((t (:inherit erc-nick-prefix-face))))
;;;;; erc-hl-nicks
   `(erc-hl-nicks-nick-vent-face ((t (:foreground ,zenbers-magenta))))
   `(erc-hl-nicks-nick-gnu-face ((t (:foreground ,zenbers-blue+2))))
   `(erc-hl-nicks-nick-andrew-face ((t (:foreground ,zenbers-red+2))))
;;;;; matrix
   `(matrix-client-date-header ((t (:foreground ,zenbers-fg-2 :background ,zenbers-bg :height 80))))
   `(matrix-client-failed-messages ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg+1))))
   `(matrix-client-last-seen ((t (:foreground ,zenbers-bg :background ,zenbers-bg))))
   `(matrix-client-metadata ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
   `(matrix-client-own-metadata ((t (:foreground ,zenbers-green :background ,zenbers-bg))))
   `(matrix-client-own-message-body ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
;;;;; eros
   `(eros-result-overlay-face ((t (:background unspecified))))
;;;;; ert
   `(ert-test-result-expected ((t (:foreground ,zenbers-green+4 :background ,zenbers-bg))))
   `(ert-test-result-unexpected ((t (:foreground ,zenbers-red :background ,zenbers-bg))))
;;;;; eshell
   `(eshell-prompt ((t (:foreground ,zenbers-green))))
   `(eshell-ls-archive ((t (:foreground ,zenbers-red-1 :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,zenbers-blue+2 :weight bold))))
   `(eshell-ls-executable ((t (:foreground ,zenbers-green+3))))
   `(eshell-ls-unreadable ((t (:foreground ,zenbers-fg))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,zenbers-yellow :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,zenbers-cyan :weight bold))))
;;;;; fancy battery
   `(fancy-battery-charging ((t (:foreground ,zenbers-green :weight bold))))
   `(fancy-battery-discharging ((t (:foreground ,zenbers-green))))
   `(fancy-battery-critical ((t (:foreground ,zenbers-red+2 :weight bold))))
;;;;; flx
   `(flx-highlight-face ((t (:foreground ,zenbers-green+2 :weight bold))))
;;;;; flymake
   `(flymake-error ((t (:background ,zenbers-red-6 :underline nil))))
   `(flymake-warning ((t (:foreground ,zenbers-blue+2 :underline nil))))
   `(flymake-note ((t (:foreground ,zenbers-green :underline nil))))
;;;;; flycheck
   `(flycheck-error ((t (:foreground ,zenbers-fg+1 :background ,zenbers-red-6 :underline nil))))
   `(flycheck-error-list-id-with-explainer ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(flycheck-error-list-highlight ((t (:background ,zenbers-bg+2))))
   `(flycheck-warning ((t (:inherit flymake-warning))))
   `(flycheck-info ((t (:foreground ,zenbers-blue+2 :underline nil))))
   `(flycheck-fringe-error ((t (:foreground ,zenbers-red-1 :weight bold))))
   `(flycheck-fringe-warning ((t (:foreground ,zenbers-yellow :weight bold))))
   `(flycheck-fringe-info ((t (:foreground ,zenbers-cyan :weight bold))))
;;;;; flyspell
   `(flyspell-duplicate ((t (:foreground ,zenbers-orange))))
   `(flyspell-incorrect ((t (:foreground ,zenbers-red+1))))
;;;;; form-feed
   `(form-feed-line ((t (:foreground ,zenbers-fg-2 :strike-through t))))
;;;;; full-ack
   `(ack-separator ((t (:foreground ,zenbers-fg))))
   `(ack-file ((t (:foreground ,zenbers-blue))))
   `(ack-line ((t (:foreground ,zenbers-yellow))))
   `(ack-match ((t (:foreground ,zenbers-orange :background ,zenbers-bg-1 :weight bold))))
;;;;; git-annex
   '(git-annex-dired-annexed-available ((t (:inherit success :weight normal))))
   '(git-annex-dired-annexed-unavailable ((t (:inherit error :weight normal))))
;;;;; git-commit
   `(git-commit-summary  ((,class (:foreground ,zenbers-fg-1 :weight bold))))
   `(git-commit-comment-action  ((,class (:foreground ,zenbers-green+1 :weight bold))))
   `(git-commit-comment-branch  ((,class (:foreground ,zenbers-blue+1  :weight bold)))) ; obsolete
   `(git-commit-comment-branch-local  ((,class (:foreground ,zenbers-blue+1  :weight bold))))
   `(git-commit-comment-branch-remote ((,class (:foreground ,zenbers-green  :weight bold))))
   `(git-commit-comment-heading ((,class (:foreground ,zenbers-yellow  :weight bold))))
;;;;; git-gutter
   `(git-gutter:added ((t (:foreground ,zenbers-green :weight bold :inverse-video t))))
   `(git-gutter:deleted ((t (:foreground ,zenbers-red :weight bold :inverse-video t))))
   `(git-gutter:modified ((t (:foreground ,zenbers-magenta :weight bold :inverse-video t))))
   `(git-gutter:unchanged ((t (:foreground ,zenbers-fg :weight bold :inverse-video t))))
;;;;; git-gutter-fr
   `(git-gutter-fr:added ((t (:foreground ,zenbers-green  :weight bold))))
   `(git-gutter-fr:deleted ((t (:foreground ,zenbers-red :weight bold))))
   `(git-gutter-fr:modified ((t (:foreground ,zenbers-magenta :weight bold))))
;;;;; git-rebase
   `(git-rebase-hash ((t (:foreground, zenbers-orange))))
;;;;; gnus
   `(gnus-group-mail-1 ((t (:weight bold :inherit gnus-group-mail-1-empty))))
   `(gnus-group-mail-1-empty ((t (:inherit gnus-group-news-1-empty))))
   `(gnus-group-mail-2 ((t (:weight bold :inherit gnus-group-mail-2-empty))))
   `(gnus-group-mail-2-empty ((t (:inherit gnus-group-news-2-empty))))
   `(gnus-group-mail-3 ((t (:weight bold :inherit gnus-group-mail-3-empty))))
   `(gnus-group-mail-3-empty ((t (:inherit gnus-group-news-3-empty))))
   `(gnus-group-mail-4 ((t (:weight bold :inherit gnus-group-mail-4-empty))))
   `(gnus-group-mail-4-empty ((t (:inherit gnus-group-news-4-empty))))
   `(gnus-group-mail-5 ((t (:weight bold :inherit gnus-group-mail-5-empty))))
   `(gnus-group-mail-5-empty ((t (:inherit gnus-group-news-5-empty))))
   `(gnus-group-mail-6 ((t (:weight bold :inherit gnus-group-mail-6-empty))))
   `(gnus-group-mail-6-empty ((t (:inherit gnus-group-news-6-empty))))
   `(gnus-group-mail-low ((t (:weight bold :inherit gnus-group-mail-low-empty))))
   `(gnus-group-mail-low-empty ((t (:inherit gnus-group-news-low-empty))))
   `(gnus-group-news-1 ((t (:weight bold :inherit gnus-group-news-1-empty))))
   `(gnus-group-news-2 ((t (:weight bold :inherit gnus-group-news-2-empty))))
   `(gnus-group-news-3 ((t (:weight bold :inherit gnus-group-news-3-empty))))
   `(gnus-group-news-4 ((t (:weight bold :inherit gnus-group-news-4-empty))))
   `(gnus-group-news-5 ((t (:weight bold :inherit gnus-group-news-5-empty))))
   `(gnus-group-news-6 ((t (:weight bold :inherit gnus-group-news-6-empty))))
   `(gnus-group-news-low ((t (:weight bold :inherit gnus-group-news-low-empty))))
   `(gnus-header-content ((t (:inherit message-header-other))))
   `(gnus-header-from ((t (:inherit message-header-to))))
   `(gnus-header-name ((t (:inherit message-header-name))))
   `(gnus-header-newsgroups ((t (:inherit message-header-other))))
   `(gnus-header-subject ((t (:inherit message-header-subject))))
   `(gnus-server-opened ((t (:foreground ,zenbers-green+2 :weight bold))))
   `(gnus-server-denied ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(gnus-server-closed ((t (:foreground ,zenbers-blue :slant italic))))
   `(gnus-server-offline ((t (:foreground ,zenbers-yellow :weight bold))))
   `(gnus-server-agent ((t (:foreground ,zenbers-blue :weight bold))))
   `(gnus-summary-cancelled ((t (:foreground ,zenbers-orange))))
   `(gnus-summary-high-ancient ((t (:foreground ,zenbers-blue))))
   `(gnus-summary-high-read ((t (:foreground ,zenbers-green :weight bold))))
   `(gnus-summary-high-ticked ((t (:foreground ,zenbers-orange :weight bold))))
   `(gnus-summary-high-unread ((t (:foreground ,zenbers-fg :weight bold))))
   `(gnus-summary-low-ancient ((t (:foreground ,zenbers-blue))))
   `(gnus-summary-low-read ((t (:foreground ,zenbers-green))))
   `(gnus-summary-low-ticked ((t (:foreground ,zenbers-orange :weight bold))))
   `(gnus-summary-low-unread ((t (:foreground ,zenbers-fg))))
   `(gnus-summary-normal-ancient ((t (:foreground ,zenbers-blue))))
   `(gnus-summary-normal-read ((t (:foreground ,zenbers-green))))
   `(gnus-summary-normal-ticked ((t (:foreground ,zenbers-orange :weight bold))))
   `(gnus-summary-normal-unread ((t (:foreground ,zenbers-fg))))
   `(gnus-summary-selected ((t (:foreground ,zenbers-yellow :weight bold))))
   `(gnus-cite-1 ((t (:foreground ,zenbers-blue))))
   `(gnus-cite-10 ((t (:foreground ,zenbers-yellow-1))))
   `(gnus-cite-11 ((t (:foreground ,zenbers-yellow))))
   `(gnus-cite-2 ((t (:foreground ,zenbers-blue-1))))
   `(gnus-cite-3 ((t (:foreground ,zenbers-blue-2))))
   `(gnus-cite-4 ((t (:foreground ,zenbers-green+2))))
   `(gnus-cite-5 ((t (:foreground ,zenbers-green+1))))
   `(gnus-cite-6 ((t (:foreground ,zenbers-green))))
   `(gnus-cite-7 ((t (:foreground ,zenbers-red))))
   `(gnus-cite-8 ((t (:foreground ,zenbers-red-1))))
   `(gnus-cite-9 ((t (:foreground ,zenbers-red-2))))
   `(gnus-group-news-1-empty ((t (:foreground ,zenbers-yellow))))
   `(gnus-group-news-2-empty ((t (:foreground ,zenbers-green+3))))
   `(gnus-group-news-3-empty ((t (:foreground ,zenbers-green+1))))
   `(gnus-group-news-4-empty ((t (:foreground ,zenbers-blue-2))))
   `(gnus-group-news-5-empty ((t (:foreground ,zenbers-blue-3))))
   `(gnus-group-news-6-empty ((t (:foreground ,zenbers-bg+2))))
   `(gnus-group-news-low-empty ((t (:foreground ,zenbers-bg+2))))
   `(gnus-signature ((t (:foreground ,zenbers-yellow))))
   `(gnus-x ((t (:background ,zenbers-fg :foreground ,zenbers-bg))))
   `(mm-uu-extract ((t (:background ,zenbers-bg-05 :foreground ,zenbers-green+1))))
;;;;; go-guru
   `(go-guru-hl-identifier-face ((t (:foreground ,zenbers-bg-1 :background ,zenbers-green+1))))
;;;;; guide-key
   `(guide-key/highlight-command-face ((t (:foreground ,zenbers-blue))))
   `(guide-key/key-face ((t (:foreground ,zenbers-green))))
   `(guide-key/prefix-command-face ((t (:foreground ,zenbers-green+1))))
;;;;; hackernews
   `(hackernews-link ((t (:foreground ,zenbers-green+2))))
   `(hackernews-link-visited ((t (:foreground ,zenbers-fg-2))))
   `(hackernews-comment-count ((t (:foreground ,zenbers-fg-2))))
   `(hackernews-score ((t (:foreground ,zenbers-fg-2))))
;;;;; haskell-mode
   `(haskell-interactive-face-prompt ((t (:inherit comint-highlight-prompt))))
   `(haskell-error-face ((t (:inherit flycheck-error))))
   `(haskell-warning-face ((t (:inherit flycheck-warning))))
;;;;; helm
   `(helm-header
     ((t (:foreground ,zenbers-green :background ,zenbers-bg))))
   `(helm-source-header
     ((t (:foreground ,zenbers-green :background ,zenbers-bg :weight bold ))))
   `(helm-selection ((t (:background ,zenbers-bg+1 :underline nil))))
   `(helm-selection-line ((t (:background ,zenbers-bg+1))))
   `(helm-visible-mark ((t (:foreground ,zenbers-blue+3 :background ,zenbers-bg :weight bold))))
   `(helm-candidate-number ((t (:foreground ,zenbers-green+4 :background ,zenbers-bg-1))))
   `(helm-separator ((t (:foreground ,zenbers-red :background ,zenbers-bg))))
   `(helm-time-zone-current ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
   `(helm-time-zone-home ((t (:foreground ,zenbers-red :background ,zenbers-bg))))
   `(helm-bookmark-addressbook ((t (:foreground ,zenbers-green+3 :background ,zenbers-bg))))
   `(helm-bookmark-directory ((t (:foreground nil :background nil :inherit helm-ff-directory))))
   `(helm-bookmark-file ((t (:foreground nil :background nil :inherit helm-ff-file))))
   `(helm-bookmark-gnus ((t (:foreground ,zenbers-magenta :background ,zenbers-bg))))
   `(helm-bookmark-info ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
   `(helm-bookmark-man ((t (:foreground ,zenbers-yellow :background ,zenbers-bg))))
   `(helm-bookmark-w3m ((t (:foreground ,zenbers-magenta :background ,zenbers-bg))))
   `(helm-buffer-not-saved ((t (:foreground ,zenbers-red :background ,zenbers-bg))))
   `(helm-buffer-process ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(helm-buffer-saved-out ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(helm-buffer-size ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(helm-buffer-directory ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg :weight bold))))
   `(helm-non-file-buffer ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(helm-buffer-file ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(helm-ff-directory ((t (:foreground ,zenbers-red :background ,zenbers-bg :weight bold))))
   `(helm-ff-file ((t (:foreground ,zenbers-fg :background ,zenbers-bg :weight normal))))
   `(helm-ff-file-extension ((t (:foreground ,zenbers-fg-2))))
   `(helm-ff-socket ((t (:foreground ,zenbers-blue+2 :background ,zenbers-bg :weight normal))))
   `(helm-ff-executable ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg :weight normal))))
   `(helm-ff-invalid-symlink ((t (:foreground ,zenbers-red :background ,zenbers-bg :weight bold))))
   `(helm-ff-symlink ((t (:foreground ,zenbers-yellow :background ,zenbers-bg :weight bold))))
   `(helm-ff-prefix ((t (:foreground ,zenbers-yellow :background ,zenbers-bg :weight bold))))
   `(helm-ff-dotted-directory ((t (:foreground ,zenbers-green+1 :background ,zenbers-bg :weight bold))))
   `(helm-ff-dotted-symlink-directory ((t (:foreground ,zenbers-green+1 :background ,zenbers-bg :weight bold))))
   `(helm-grep-cmd-line ((t (:foreground ,zenbers-cyan :background ,zenbers-bg))))
   `(helm-grep-file ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(helm-grep-finish ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
   `(helm-grep-lineno ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(helm-grep-match ((t (:foreground nil :background nil :inherit helm-match))))
   `(helm-grep-running ((t (:foreground ,zenbers-red :background ,zenbers-bg))))
   `(helm-match ((t (:foreground ,zenbers-green+4 :background ,zenbers-bg :weight bold))))
   `(helm-moccur-buffer ((t (:foreground ,zenbers-cyan :background ,zenbers-bg))))
   `(helm-mu-contacts-address-face ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(helm-mu-contacts-name-face ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(helm-M-x-key ((t (:foreground ,zenbers-cyan))))
   `(helm-mode-prefix ((t (:foreground ,zenbers-cyan))))
;;;;; helm-swoop
   `(helm-swoop-target-line-face ((t (:foreground ,zenbers-fg :background ,zenbers-bg))))
   `(helm-swoop-target-word-face ((t (:foreground ,zenbers-green+4 :background ,zenbers-bg :weight bold))))
;;;;; hexl-mode
   `(hexl-address-region ((t (:foreground ,zenbers-green))))
   `(hexl-ascii-region ((t (:foreground ,zenbers-fg :background ,zenbers-bg+1))))
;;;;; switch-window
   `(switch-window-label ((t (:foreground ,zenbers-green+3 :background ,zenbers-bg))))
   `(switch-window-background ((t (:foreground ,zenbers-bg+3 :background ,zenbers-bg))))
;;;;; hl-line-mode
   `(hl-line ((t (:background ,zenbers-bg+1))))
;;;;; hl-sexp
   `(hl-sexp-face ((,class (:background ,zenbers-bg+1))
                   (t :weight bold)))
;;;;; hydra
   `(hydra-face-red ((t (:foreground ,zenbers-red-1 :background ,zenbers-bg))))
   `(hydra-face-amaranth ((t (:foreground ,zenbers-red-3 :background ,zenbers-bg))))
   `(hydra-face-blue ((t (:foreground ,zenbers-blue :background ,zenbers-bg))))
   `(hydra-face-pink ((t (:foreground ,zenbers-magenta :background ,zenbers-bg))))
   `(hydra-face-teal ((t (:foreground ,zenbers-cyan :background ,zenbers-bg))))
;;;;; info+
   `(info-command-ref-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-orange))))
   `(info-constant-ref-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-magenta))))
   `(info-double-quoted-name ((t (:inherit font-lock-comment-face))))
   `(info-file ((t (:background ,zenbers-bg-1 :foreground ,zenbers-yellow))))
   `(info-function-ref-item ((t (:background ,zenbers-bg-1 :inherit font-lock-function-name-face))))
   `(info-macro-ref-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-yellow))))
   `(info-menu ((t (:foreground ,zenbers-yellow))))
   `(info-quoted-name ((t (:inherit font-lock-constant-face))))
   `(info-reference-item ((t (:background ,zenbers-bg-1))))
   `(info-single-quote ((t (:inherit font-lock-keyword-face))))
   `(info-special-form-ref-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-yellow))))
   `(info-string ((t (:inherit font-lock-string-face))))
   `(info-syntax-class-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-blue+1))))
   `(info-user-option-ref-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-red))))
   `(info-variable-ref-item ((t (:background ,zenbers-bg-1 :foreground ,zenbers-orange))))
   `(info-title-1 ((t (:foreground ,zenbers-green :background ,zenbers-bg :height 160))))
;;;;; irfc
   `(irfc-head-name-face ((t (:foreground ,zenbers-red :weight bold))))
   `(irfc-head-number-face ((t (:foreground ,zenbers-red :weight bold))))
   `(irfc-reference-face ((t (:foreground ,zenbers-blue-1 :weight bold))))
   `(irfc-requirement-keyword-face ((t (:inherit font-lock-keyword-face))))
   `(irfc-rfc-link-face ((t (:inherit link))))
   `(irfc-rfc-number-face ((t (:foreground ,zenbers-cyan :weight bold))))
   `(irfc-std-number-face ((t (:foreground ,zenbers-green+4 :weight bold))))
   `(irfc-table-item-face ((t (:foreground ,zenbers-green+3))))
   `(irfc-title-face ((t (:foreground ,zenbers-yellow :underline t :weight bold))))
;;;;; ivy
   `(ivy-confirm-face ((t (:foreground ,zenbers-green :background ,zenbers-bg))))
   `(ivy-current-match ((t (:inherit match))))
   `(ivy-cursor ((t (:foreground ,zenbers-bg :background ,zenbers-fg))))
   `(ivy-match-required-face ((t (:foreground ,zenbers-red :background ,zenbers-bg))))
   `(ivy-minibuffer-match-face-1 ((t (:foreground ,zenbers-fg-1))))
   `(ivy-minibuffer-match-face-2 ((t (:foreground ,zenbers-fg-1))))
   `(ivy-minibuffer-match-face-3 ((t (:foreground ,zenbers-fg-1))))
   `(ivy-minibuffer-match-face-4 ((t (:foreground ,zenbers-fg-1))))
   `(ivy-highlight-face ((t (:foreground ,zenbers-fg))))
   `(ivy-remote ((t (:foreground ,zenbers-red+2))))
   `(ivy-subdir ((t (:foreground ,zenbers-green :weight bold))))
   `(ivy-org ((t (:inherit default))))
   `(ivy-modified-buffer ((t (:foreground ,zenbers-fg))))
   `(ivy-virtual ((t (:foreground ,zenbers-fg-2))))
   `(ivy-completions-annotations ((t (:foreground ,zenbers-fg-2))))
;;;;; ivy-posframe
   `(ivy-posframe ((t (:foreground ,zenbers-fg :background ,zenbers-bg+05))))
   `(ivy-posframe-border ((t (:foreground ,zenbers-fg :background ,zenbers-fg))))
;;;;; counsel
   `(counsel-key-binding ((t (:foreground ,zenbers-blue+3))))
;;;;; swiper
   `(swiper-line-face ((t (:background ,zenbers-bg+3))))
;;;;; ido-mode
   `(ido-first-match ((t (:foreground ,zenbers-yellow :weight bold))))
   `(ido-only-match ((t (:foreground ,zenbers-orange :weight bold))))
   `(ido-subdir ((t (:foreground ,zenbers-yellow))))
   `(ido-indicator ((t (:foreground ,zenbers-yellow :background ,zenbers-red-4))))
;;;;; iedit-mode
   `(iedit-occurrence ((t (:background ,zenbers-bg+2 :weight bold))))
;;;;; jabber-mode
   `(jabber-roster-user-away ((t (:foreground ,zenbers-green+2))))
   `(jabber-roster-user-online ((t (:foreground ,zenbers-blue-1))))
   `(jabber-roster-user-dnd ((t (:foreground ,zenbers-red+1))))
   `(jabber-roster-user-xa ((t (:foreground ,zenbers-magenta))))
   `(jabber-roster-user-chatty ((t (:foreground ,zenbers-orange))))
   `(jabber-roster-user-error ((t (:foreground ,zenbers-red+1))))
   `(jabber-rare-time-face ((t (:foreground ,zenbers-green+1))))
   `(jabber-chat-prompt-local ((t (:foreground ,zenbers-blue-1))))
   `(jabber-chat-prompt-foreign ((t (:foreground ,zenbers-red+1))))
   `(jabber-chat-prompt-system ((t (:foreground ,zenbers-green+3))))
   `(jabber-activity-face((t (:foreground ,zenbers-red+1))))
   `(jabber-activity-personal-face ((t (:foreground ,zenbers-blue+1))))
   `(jabber-title-small ((t (:height 1.1 :weight bold))))
   `(jabber-title-medium ((t (:height 1.2 :weight bold))))
   `(jabber-title-large ((t (:height 1.3 :weight bold))))
;;;;; jinx
   `(jinx-misspelled ((t (:underline (:color ,zenbers-red-2)))))
;;;;; js2-mode
   `(js2-warning ((t (:underline ,zenbers-orange))))
   `(js2-error ((t (:foreground ,zenbers-red :weight bold))))
   `(js2-jsdoc-tag ((t (:foreground ,zenbers-green-2))))
   `(js2-jsdoc-type ((t (:foreground ,zenbers-green+2))))
   `(js2-jsdoc-value ((t (:foreground ,zenbers-green+3))))
   `(js2-function-param ((t (:foreground, zenbers-orange))))
   `(js2-external-variable ((t (:foreground ,zenbers-orange))))
;;;; JDEE
   `(jdee-font-lock-package-face ((t (:foreground ,zenbers-fg))))
   `(jdee-font-lock-private-face ((t (:foreground ,zenbers-fg-1))))
   `(jdee-font-lock-public-face ((t (:foreground ,zenbers-fg-1))))
   `(jdee-font-lock-modifier-face ((t (:foreground ,zenbers-fg-1))))
   `(jdee-font-lock-constant-face ((t (:foreground ,zenbers-red+2))))
   `(jdee-font-lock-constructor-face ((t (:foreground ,zenbers-fg :weight bold))))
   `(jdee-font-lock-number-face ((t (:inherit highlight-numbers-number))))
   `(jdee-font-lock-doc-tag-face ((t (:foreground ,zenbers-red+1))))
;;;; additional js2 mode attributes for better syntax highlighting
   `(js2-instance-member ((t (:foreground ,zenbers-green-2))))
   `(js2-jsdoc-html-tag-delimiter ((t (:foreground ,zenbers-orange))))
   `(js2-jsdoc-html-tag-name ((t (:foreground ,zenbers-red-1))))
   `(js2-object-property ((t (:foreground ,zenbers-blue+1))))
   `(js2-magic-paren ((t (:foreground ,zenbers-blue-5))))
   `(js2-private-function-call ((t (:foreground ,zenbers-cyan))))
   `(js2-function-call ((t (:foreground ,zenbers-cyan))))
   `(js2-private-member ((t (:foreground ,zenbers-blue-1))))
   `(js2-keywords ((t (:foreground ,zenbers-magenta))))
;;;;; ledger-mode
   `(ledger-font-payee-uncleared-face ((t (:foreground ,zenbers-red-1 :weight bold))))
   `(ledger-font-payee-cleared-face ((t (:foreground ,zenbers-fg :weight normal))))
   `(ledger-font-payee-pending-face ((t (:foreground ,zenbers-red :weight normal))))
   `(ledger-font-xact-highlight-face ((t (:background ,zenbers-bg+1))))
   `(ledger-font-auto-xact-face ((t (:foreground ,zenbers-yellow-1 :weight normal))))
   `(ledger-font-periodic-xact-face ((t (:foreground ,zenbers-green :weight normal))))
   `(ledger-font-pending-face ((t (:foreground ,zenbers-orange weight: normal))))
   `(ledger-font-other-face ((t (:foreground ,zenbers-fg))))
   `(ledger-font-posting-date-face ((t (:foreground ,zenbers-orange :weight normal))))
   `(ledger-font-posting-account-face ((t (:foreground ,zenbers-blue-1))))
   `(ledger-font-posting-account-cleared-face ((t (:foreground ,zenbers-fg))))
   `(ledger-font-posting-account-pending-face ((t (:foreground ,zenbers-orange))))
   `(ledger-font-posting-amount-face ((t (:foreground ,zenbers-orange))))
   `(ledger-occur-narrowed-face ((t (:foreground ,zenbers-fg-1 :invisible t))))
   `(ledger-occur-xact-face ((t (:background ,zenbers-bg+1))))
   `(ledger-font-comment-face ((t (:foreground ,zenbers-green))))
   `(ledger-font-reconciler-uncleared-face ((t (:foreground ,zenbers-red-1 :weight bold))))
   `(ledger-font-reconciler-cleared-face ((t (:foreground ,zenbers-fg :weight normal))))
   `(ledger-font-reconciler-pending-face ((t (:foreground ,zenbers-orange :weight normal))))
   `(ledger-font-report-clickable-face ((t (:foreground ,zenbers-orange :weight normal))))
;;;;; linum-mode
   `(linum ((t (:foreground ,zenbers-fg-2 :background ,zenbers-bg+1))))
;;;;; lispy
   `(lispy-command-name-face ((t (:background ,zenbers-bg-05 :inherit font-lock-function-name-face))))
   `(lispy-cursor-face ((t (:foreground ,zenbers-bg :background ,zenbers-fg))))
   `(lispy-face-hint ((t (:inherit highlight :foreground ,zenbers-yellow))))
;;;;; LSP
   `(lsp-face-highlight-read ((t (:background ,zenbers-bg+1))))
   `(lsp-face-highlight-write ((t (:background ,zenbers-bg+1))))
   `(lsp-face-highlight-textual ((t (:background ,zenbers-bg+2))))
   `(lsp-ui-doc-background ((t (:background ,zenbers-bg+1))))
   `(lsp-ui-doc-header ((t (:foreground ,zenbers-green :background ,zenbers-bg+05 :weight bold))))
   `(markdown-code-face ((t (:foreground ,zenbers-red+2))))
   `(lsp-ui-peek-header ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg+2))))
   `(lsp-ui-peek-footer ((t (:background ,zenbers-bg+05))))
   `(lsp-ui-peek-highlight ((t (:foreground ,zenbers-green+4 :background ,zenbers-bg+05))))
   `(lsp-ui-peek-selection ((t (:background ,zenbers-bg+2))))
   `(lsp-ui-peek-list ((t (:background ,zenbers-bg+05))))
   `(lsp-ui-peek-peek ((t (:background ,zenbers-bg+05))))
   `(lsp-ui-peek-filename ((t (:foreground ,zenbers-bg+3))))
   `(lsp-ui-peek-line-number ((t (:foreground ,zenbers-bg+3))))
   `(lsp-ui-sideline-current-symbol ((t (:foreground ,zenbers-green+2 :weight bold :box nil))))
   `(lsp-ui-sideline-symbol ((t (:foreground ,zenbers-green+4 :box nil))))
   `(lsp-ui-sideline-code-action ((t (:foreground ,zenbers-blue+3))))
   `(lsp-lsp-flycheck-warning-unnecessary-face ((t (:inherit flycheck-warning))))
;;;;; lui
   `(lui-time-stamp-face ((t (:foreground ,zenbers-blue-1))))
   `(lui-hilight-face ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
   `(lui-button-face ((t (:inherit hover-highlight))))
;;;;; macrostep
   `(macrostep-gensym-1
     ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg-1))))
   `(macrostep-gensym-2
     ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg-1))))
   `(macrostep-gensym-3
     ((t (:foreground ,zenbers-blue+1 :background ,zenbers-bg-1))))
   `(macrostep-gensym-4
     ((t (:foreground ,zenbers-magenta :background ,zenbers-bg-1))))
   `(macrostep-gensym-5
     ((t (:foreground ,zenbers-yellow :background ,zenbers-bg-1))))
   `(macrostep-expansion-highlight-face
     ((t (:inherit highlight))))
   `(macrostep-macro-face
     ((t (:underline t))))
;;;;; magit
;;;;;; headings and diffs
   `(magit-header-line           ((t (:foreground ,zenbers-green+3 :background ,zenbers-bg :weight bold))))

   `(magit-section-highlight           ((t (:background ,zenbers-bg+1 :box nil))))
   `(magit-section-heading             ((t (:foreground ,zenbers-green :weight bold :box nil))))
   `(magit-section-heading-selection   ((t (:foreground ,zenbers-green+1 :weight bold :box nil))))

   `(magit-diff-added ((t (:inherit diff-added))))
   `(magit-diff-added-highlight ((t (:inherit diff-refine-added))))
   `(magit-diff-removed ((t (:inherit diff-removed))))
   `(magit-diff-removed-highlight ((t (:inherit diff-refine-removed))))

   `(magit-diff-file-heading           ((t (:background ,zenbers-bg+1 :weight bold))))
   `(magit-diff-file-heading-highlight ((t (:background ,zenbers-bg+05  :weight bold))))
   `(magit-diff-file-heading-selection ((t (:background ,zenbers-bg+05
                                                        :foreground ,zenbers-orange :weight bold))))
   `(magit-diff-hunk-heading           ((t (:background ,zenbers-bg+1))))
   `(magit-diff-hunk-heading-highlight ((t (:background ,zenbers-bg+2))))
   `(magit-diff-hunk-heading-selection ((t (:background ,zenbers-bg+2
                                                        :foreground ,zenbers-orange))))
   `(magit-diff-lines-heading          ((t (:background ,zenbers-orange
                                                        :foreground ,zenbers-bg+2))))
   `(magit-diff-context-highlight      ((t (:background ,zenbers-bg+05
                                                        :foreground "grey70"))))
   `(magit-diffstat-added   ((t (:inherit diff-added))))
   `(magit-diffstat-removed ((t (:inherit diff-removed))))
;;;;;; page-break-lines
   `(page-break-lines ((t (:foreground ,zenbers-fg-2))))
;;;;;; popup
   `(magit-popup-heading             ((t (:foreground ,zenbers-yellow  :weight bold))))
   `(magit-popup-key                 ((t (:foreground ,zenbers-green-2 :weight bold))))
   `(magit-popup-argument            ((t (:foreground ,zenbers-green   :weight bold))))
   `(magit-popup-disabled-argument   ((t (:foreground ,zenbers-fg-1    :weight normal))))
   `(magit-popup-option-value        ((t (:foreground ,zenbers-blue-2  :weight bold))))
;;;;;; process
   `(magit-process-ok    ((t (:foreground ,zenbers-green  :weight bold))))
   `(magit-process-ng    ((t (:foreground ,zenbers-red    :weight bold))))
;;;;;; log
   `(magit-log-author    ((t (:foreground ,zenbers-orange))))
   `(magit-log-date      ((t (:foreground ,zenbers-fg-1))))
   `(magit-log-graph     ((t (:foreground ,zenbers-fg+1))))
;;;;;; sequence
   `(magit-sequence-pick ((t (:foreground ,zenbers-yellow-2))))
   `(magit-sequence-stop ((t (:foreground ,zenbers-green))))
   `(magit-sequence-part ((t (:foreground ,zenbers-yellow))))
   `(magit-sequence-head ((t (:foreground ,zenbers-blue))))
   `(magit-sequence-drop ((t (:foreground ,zenbers-red))))
   `(magit-sequence-done ((t (:foreground ,zenbers-fg-1))))
   `(magit-sequence-onto ((t (:foreground ,zenbers-fg-1))))
;;;;;; bisect
   `(magit-bisect-good ((t (:foreground ,zenbers-green))))
   `(magit-bisect-skip ((t (:foreground ,zenbers-yellow))))
   `(magit-bisect-bad  ((t (:foreground ,zenbers-red))))
;;;;;; blame
   `(magit-blame-heading ((t (:background ,zenbers-bg-1 :foreground ,zenbers-blue-2))))
   `(magit-blame-hash    ((t (:background ,zenbers-bg-1 :foreground ,zenbers-blue-2))))
   `(magit-blame-name    ((t (:background ,zenbers-bg-1 :foreground ,zenbers-orange))))
   `(magit-blame-date    ((t (:background ,zenbers-bg-1 :foreground ,zenbers-orange))))
   `(magit-blame-summary ((t (:background ,zenbers-bg-1 :foreground ,zenbers-blue-2
                                          :weight bold))))
;;;;;; references etc
   `(magit-dimmed         ((t (:foreground ,zenbers-bg+3))))
   `(magit-hash           ((t (:foreground ,zenbers-bg+3))))
   `(magit-tag            ((t (:foreground ,zenbers-orange :weight bold))))
   `(magit-branch-remote  ((t (:foreground ,zenbers-magenta  :weight bold :box nil))))
   `(magit-branch-remote-head  ((t (:foreground ,zenbers-magenta  :weight bold :box nil))))
   `(magit-branch-local   ((t (:foreground ,zenbers-blue+1 :weight bold))))
   `(magit-branch-current ((t (:foreground ,zenbers-blue+1 :weight bold))))
   `(magit-head           ((t (:foreground ,zenbers-blue+1 :weight bold))))
   `(magit-refname        ((t (:background ,zenbers-bg+2 :foreground ,zenbers-fg :weight bold))))
   `(magit-refname-stash  ((t (:background ,zenbers-bg+2 :foreground ,zenbers-fg :weight bold))))
   `(magit-refname-wip    ((t (:background ,zenbers-bg+2 :foreground ,zenbers-fg :weight bold))))
   `(magit-signature-good      ((t (:foreground ,zenbers-green))))
   `(magit-signature-bad       ((t (:foreground ,zenbers-red))))
   `(magit-signature-untrusted ((t (:foreground ,zenbers-yellow))))
   `(magit-signature-expired   ((t (:foreground ,zenbers-orange))))
   `(magit-signature-revoked   ((t (:foreground ,zenbers-magenta))))
   '(magit-signature-error     ((t (:inherit    magit-signature-bad))))
   `(magit-cherry-unmatched    ((t (:foreground ,zenbers-cyan))))
   `(magit-cherry-equivalent   ((t (:foreground ,zenbers-magenta))))
   `(magit-reflog-commit       ((t (:foreground ,zenbers-green))))
   `(magit-reflog-amend        ((t (:foreground ,zenbers-magenta))))
   `(magit-reflog-merge        ((t (:foreground ,zenbers-green))))
   `(magit-reflog-checkout     ((t (:foreground ,zenbers-blue))))
   `(magit-reflog-reset        ((t (:foreground ,zenbers-red))))
   `(magit-reflog-rebase       ((t (:foreground ,zenbers-magenta))))
   `(magit-reflog-cherry-pick  ((t (:foreground ,zenbers-green))))
   `(magit-reflog-remote       ((t (:foreground ,zenbers-cyan))))
   `(magit-reflog-other        ((t (:foreground ,zenbers-cyan))))
;;;;; marginalia
   `(marginalia-date ((t (:foreground ,zenbers-fg-2))))
   `(marginalia-size ((t (:foreground ,zenbers-red+1))))
   `(marginalia-mode ((t (:foreground ,zenbers-green :weight bold))))
   `(marginalia-modified ((t (:foreground ,zenbers-fg-1 :weight bold))))
   `(marginalia-key ((t (:foreground ,zenbers-blue+2))))
   `(marginalia-type ((t (:inherit font-lock-type-face))))
   `(marginalia-file-name ((t (:foreground ,zenbers-fg-1))))
   `(marginalia-file-priv-dir ((t (:foreground ,zenbers-fg-1))))
   `(marginalia-file-priv-read ((t (:foreground ,zenbers-red+2))))
   `(marginalia-file-priv-write ((t (:foreground ,zenbers-blue+2))))
   `(marginalia-file-priv-exec ((t (:foreground ,zenbers-green+3))))
   `(marginalia-file-priv-other ((t (:foreground ,zenbers-green :weight bold))))
;;;;; markup-faces
   `(markup-anchor-face ((t (:foreground ,zenbers-blue+1))))
   `(markup-code-face ((t (:inherit font-lock-constant-face))))
   `(markup-command-face ((t (:foreground ,zenbers-yellow))))
   `(markup-emphasis-face ((t (:inherit bold))))
   `(markup-internal-reference-face ((t (:foreground ,zenbers-yellow-2 :underline t))))
   `(markup-list-face ((t (:foreground ,zenbers-fg+1))))
   `(markup-meta-face ((t (:foreground ,zenbers-yellow))))
   `(markup-meta-hide-face ((t (:foreground ,zenbers-yellow))))
   `(markup-secondary-text-face ((t (:foreground ,zenbers-yellow-1))))
   `(markup-title-0-face ((t (:inherit font-lock-function-name-face :weight bold))))
   `(markup-title-1-face ((t (:inherit font-lock-function-name-face :weight bold))))
   `(markup-title-2-face ((t (:inherit font-lock-function-name-face :weight bold))))
   `(markup-title-3-face ((t (:inherit font-lock-function-name-face :weight bold))))
   `(markup-title-4-face ((t (:inherit font-lock-function-name-face :weight bold))))
   `(markup-typewriter-face ((t (:inherit font-lock-constant-face))))
   `(markup-verbatim-face ((t (:foreground ,zenbers-red))))
   `(markup-value-face ((t (:foreground ,zenbers-yellow))))
;;;;; mastodon
   `(mastodon-handle-face ((t (:foreground ,zenbers-fg-1))))
   `(mastodon-display-name-face ((t (:foreground ,zenbers-green :weight bold))))
;;;;; message-mode
   `(message-cited-text ((t (:foreground ,zenbers-blue+1))))
   `(message-cited-text-1 ((t (:foreground ,zenbers-blue+1))))
   `(message-cited-text-2 ((t (:foreground ,zenbers-red+1))))
   `(message-cited-text-3 ((t (:foreground ,zenbers-red-2))))
   `(message-header-name ((t (:foreground ,zenbers-orange :weight bold))))
   `(message-header-other ((t (:foreground ,zenbers-green))))
   `(message-header-to ((t (:foreground ,zenbers-fg :weight bold))))
   `(message-header-cc ((t (:foreground ,zenbers-fg-2 :weight bold))))
   `(message-header-newsgroups ((t (:foreground ,zenbers-yellow :weight bold))))
   `(message-header-subject ((t (:foreground ,zenbers-fg :weight bold))))
   `(message-header-xheader ((t (:foreground ,zenbers-green))))
   `(message-mml ((t (:foreground ,zenbers-yellow :weight bold))))
   `(message-separator ((t (:inherit font-lock-comment-face))))
;;;;; mew
   `(mew-face-header-subject ((t (:foreground ,zenbers-orange))))
   `(mew-face-header-from ((t (:foreground ,zenbers-yellow))))
   `(mew-face-header-date ((t (:foreground ,zenbers-green))))
   `(mew-face-header-to ((t (:foreground ,zenbers-red))))
   `(mew-face-header-key ((t (:foreground ,zenbers-green))))
   `(mew-face-header-private ((t (:foreground ,zenbers-green))))
   `(mew-face-header-important ((t (:foreground ,zenbers-blue))))
   `(mew-face-header-marginal ((t (:foreground ,zenbers-fg :weight bold))))
   `(mew-face-header-warning ((t (:foreground ,zenbers-red))))
   `(mew-face-header-xmew ((t (:foreground ,zenbers-green))))
   `(mew-face-header-xmew-bad ((t (:foreground ,zenbers-red))))
   `(mew-face-body-url ((t (:foreground ,zenbers-orange))))
   `(mew-face-body-comment ((t (:foreground ,zenbers-fg :slant italic))))
   `(mew-face-body-cite1 ((t (:foreground ,zenbers-green))))
   `(mew-face-body-cite2 ((t (:foreground ,zenbers-blue))))
   `(mew-face-body-cite3 ((t (:foreground ,zenbers-orange))))
   `(mew-face-body-cite4 ((t (:foreground ,zenbers-yellow))))
   `(mew-face-body-cite5 ((t (:foreground ,zenbers-red))))
   `(mew-face-mark-review ((t (:foreground ,zenbers-blue))))
   `(mew-face-mark-escape ((t (:foreground ,zenbers-green))))
   `(mew-face-mark-delete ((t (:foreground ,zenbers-red))))
   `(mew-face-mark-unlink ((t (:foreground ,zenbers-yellow))))
   `(mew-face-mark-refile ((t (:foreground ,zenbers-green))))
   `(mew-face-mark-unread ((t (:foreground ,zenbers-red-2))))
   `(mew-face-eof-message ((t (:foreground ,zenbers-green))))
   `(mew-face-eof-part ((t (:foreground ,zenbers-yellow))))
;;;;; mic-paren
   `(paren-face-match ((t (:foreground ,zenbers-cyan :background ,zenbers-bg :weight bold))))
   `(paren-face-mismatch ((t (:foreground ,zenbers-bg :background ,zenbers-magenta :weight bold))))
   `(paren-face-no-match ((t (:foreground ,zenbers-bg :background ,zenbers-red :weight bold))))
;;;;; minimap
   `(minimap-active-region-background ((t (:background ,zenbers-bg+1))))
;;;;; mingus
   `(mingus-directory-face ((t (:foreground ,zenbers-green+1))))
   `(mingus-pausing-face ((t (:foreground ,zenbers-magenta))))
   `(mingus-playing-face ((t (:foreground ,zenbers-cyan))))
   `(mingus-playlist-face ((t (:foreground ,zenbers-cyan ))))
   `(mingus-mark-face ((t (:bold t :foreground ,zenbers-magenta))))
   `(mingus-song-file-face ((t (:foreground ,zenbers-fg-1))))
   `(mingus-artist-face ((t (:foreground ,zenbers-green+1))))
   `(mingus-album-face ((t (:underline t :foreground ,zenbers-red+1))))
   `(mingus-album-stale-face ((t (:foreground ,zenbers-red+1))))
   `(mingus-stopped-face ((t (:foreground ,zenbers-red))))
;;;;; nav
   `(nav-face-heading ((t (:foreground ,zenbers-yellow))))
   `(nav-face-button-num ((t (:foreground ,zenbers-cyan))))
   `(nav-face-dir ((t (:foreground ,zenbers-green))))
   `(nav-face-hdir ((t (:foreground ,zenbers-red))))
   `(nav-face-file ((t (:foreground ,zenbers-fg))))
   `(nav-face-hfile ((t (:foreground ,zenbers-red-4))))
;;;;; merlin
   `(merlin-type-face ((t (:inherit highlight))))
   `(merlin-compilation-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenbers-orange)))
      (t
       (:underline ,zenbers-orange))))
   `(merlin-compilation-error-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenbers-red)))
      (t
       (:underline ,zenbers-red))))
;;;;; mu4e
   `(mu4e-header-highlight-face ((t (:background ,zenbers-bg+2))))
   `(mu4e-header-face ((t (:foreground ,zenbers-fg-1 :background ,zenbers-bg))))
   `(mu4e-highlight-face ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(mu4e-title-face ((t (:foreground ,zenbers-green :weight bold))))
   `(mu4e-footer-face ((t (:foreground ,zenbers-fg-2))))
   `(mu4e-modeline-face ((t (:foreground ,zenbers-bg-05))))
   `(mu4e-context-face ((t (:foreground ,zenbers-green+2))))

   `(mu4e-cited-1-face ((t (:foreground ,zenbers-blue    :slant italic))))
   `(mu4e-cited-2-face ((t (:foreground ,zenbers-green+2 :slant italic))))
   `(mu4e-cited-3-face ((t (:foreground ,zenbers-blue-2  :slant italic))))
   `(mu4e-cited-4-face ((t (:foreground ,zenbers-green   :slant italic))))
   `(mu4e-cited-5-face ((t (:foreground ,zenbers-blue-4  :slant italic))))
   `(mu4e-cited-6-face ((t (:foreground ,zenbers-green-2 :slant italic))))
   `(mu4e-cited-7-face ((t (:foreground ,zenbers-blue    :slant italic))))
   `(mu4e-replied-face ((t (:foreground ,zenbers-fg-2))))
   `(mu4e-trashed-face ((t (:foreground ,zenbers-bg+3 :strike-through t))))
   `(mu4e-unread-face ((t (:foreground ,zenbers-red+2 :weight bold))))
;;;;; mumamo
   `(mumamo-background-chunk-major ((t (:background nil))))
   `(mumamo-background-chunk-submode1 ((t (:background ,zenbers-bg-1))))
   `(mumamo-background-chunk-submode2 ((t (:background ,zenbers-bg+2))))
   `(mumamo-background-chunk-submode3 ((t (:background ,zenbers-bg+3))))
   `(mumamo-background-chunk-submode4 ((t (:background ,zenbers-bg+1))))
;;;;; neotree
   `(neo-banner-face ((t (:foreground ,zenbers-blue+1 :weight bold))))
   `(neo-header-face ((t (:foreground ,zenbers-fg))))
   `(neo-root-dir-face ((t (:foreground ,zenbers-blue+1 :weight bold))))
   `(neo-dir-link-face ((t (:foreground ,zenbers-blue))))
   `(neo-file-link-face ((t (:foreground ,zenbers-fg))))
   `(neo-expand-btn-face ((t (:foreground ,zenbers-blue))))
   `(neo-vc-default-face ((t (:foreground ,zenbers-fg+1))))
   `(neo-vc-user-face ((t (:foreground ,zenbers-red :slant italic))))
   `(neo-vc-up-to-date-face ((t (:foreground ,zenbers-fg))))
   `(neo-vc-edited-face ((t (:foreground ,zenbers-magenta))))
   `(neo-vc-needs-merge-face ((t (:foreground ,zenbers-red+1))))
   `(neo-vc-unlocked-changes-face ((t (:foreground ,zenbers-red :background ,zenbers-blue-5))))
   `(neo-vc-added-face ((t (:foreground ,zenbers-green+1))))
   `(neo-vc-conflict-face ((t (:foreground ,zenbers-red+1))))
   `(neo-vc-missing-face ((t (:foreground ,zenbers-red+1))))
   `(neo-vc-ignored-face ((t (:foreground ,zenbers-fg-1))))
;;;;; orderless
   `(orderless-match-face-0 ((t (:foreground ,zenbers-fg))))
   `(orderless-match-face-1 ((t (:foreground ,zenbers-fg))))
   `(orderless-match-face-2 ((t (:foreground ,zenbers-fg))))
   `(orderless-match-face-3 ((t (:foreground ,zenbers-fg))))
;;;;; org-mode
   `(org-agenda-calendar-event ((t (:foreground ,zenbers-blue+1))))
   `(org-agenda-date ((t (:foreground ,zenbers-green :weight bold))))
   `(org-agenda-date-today ((t (:foreground ,zenbers-green+4 :weight bold))))
   `(org-agenda-date-weekend ((t (:foreground ,zenbers-green :weight bold))))
   `(org-agenda-structure ((t (:inherit font-lock-comment-face))))
   `(org-agenda-diary ((t (:foreground ,zenbers-blue+1))))
   `(org-agenda-done ((t (:foreground ,zenbers-green))))
   `(org-block ((t (:foreground ,zenbers-fg-1))))
   `(org-block-begin-line ((t (:foreground ,zenbers-green))))
   `(org-block-end-line ((t (:foreground ,zenbers-green))))
   `(org-meta-line ((t (:foreground ,zenbers-green))))
   `(org-verbatim ((t (:foreground ,zenbers-fg-1))))
   `(org-archived ((t (:foreground ,zenbers-fg-2))))
   `(org-checkbox ((t (:background ,zenbers-bg+2 :foreground ,zenbers-fg))))
   `(org-date ((t (:foreground ,zenbers-blue :underline t))))
   `(org-deadline-announce ((t (:foreground ,zenbers-red-1))))
   `(org-dispatcher-highlight ((t (:foreground ,zenbers-blue+3 :weight bold))))
   `(org-done ((t (:weight bold :weight bold :foreground ,zenbers-green))))
   `(org-formula ((t (:foreground ,zenbers-yellow-2))))
   `(org-headline-done ((t (:foreground ,zenbers-green+3))))
   `(org-hide ((t (:foreground ,zenbers-bg))))
   `(org-level-1 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-orange))))
   `(org-level-2 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-blue+1))))
   `(org-level-3 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-red+1))))
   `(org-level-4 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-red-2))))
   `(org-level-5 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-red-3))))
   `(org-level-6 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-blue-2))))
   `(org-level-7 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-red-3))))
   `(org-level-8 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-magenta))))
   `(org-link ((t (:foreground ,zenbers-blue+2 :underline t))))
   `(org-scheduled ((t (:foreground ,zenbers-blue+1))))
   `(org-scheduled-previously ((t (:foreground ,zenbers-red))))
   `(org-scheduled-today ((t (:foreground ,zenbers-blue+1))))
   `(org-sexp-date ((t (:foreground ,zenbers-blue+1 :underline t))))
   `(org-special-keyword ((t (:inherit font-lock-comment-face))))
   `(org-table ((t (:foreground ,zenbers-green+2))))
   `(org-tag ((t (:foreground ,zenbers-fg-1 :weight bold))))
   `(org-time-grid ((t (:foreground ,zenbers-fg-2))))
   `(org-agenda-current-time ((t (:foreground ,zenbers-fg-1))))
   `(org-todo ((t (:weight bold :foreground ,zenbers-red :weight bold))))
   `(org-upcoming-deadline ((t (:foreground ,zenbers-red+2 :weight bold))))
   `(org-warning ((t (:weight bold :foreground ,zenbers-red :weight bold :underline nil))))
   `(org-column ((t (:background ,zenbers-bg))))
   `(org-column-title ((t (:background ,zenbers-bg-1 :underline t :weight bold))))
   `(org-mode-line-clock ((t (:foreground ,zenbers-fg-2))))
   `(org-mode-line-clock-overrun ((t (:foreground ,zenbers-red+1))))
   `(org-ellipsis ((t (:foreground ,zenbers-yellow-1 :underline t))))
   `(org-footnote ((t (:foreground ,zenbers-cyan :underline t))))
   `(org-document-title ((t (:inherit ,z-variable-pitch :foreground ,zenbers-red+2
                                      :weight bold :height ,zenbers-height-plus-4))))
   `(org-document-info ((t (:foreground ,zenbers-blue))))
   `(org-habit-ready-face ((t :background ,zenbers-green)))
   `(org-habit-alert-face ((t :background ,zenbers-yellow-1 :foreground ,zenbers-bg)))
   `(org-habit-clear-face ((t :background ,zenbers-blue-3)))
   `(org-habit-overdue-face ((t :background ,zenbers-red-3)))
   `(org-habit-clear-future-face ((t :background ,zenbers-blue-4)))
   `(org-habit-ready-future-face ((t :background ,zenbers-green-2)))
   `(org-habit-alert-future-face ((t :background ,zenbers-yellow-2 :foreground ,zenbers-bg)))
   `(org-habit-overdue-future-face ((t :background ,zenbers-red-4)))
;;;;; org-ref
   `(org-ref-ref-face ((t :underline t)))
   `(org-ref-label-face ((t :underline t)))
   `(org-ref-cite-face ((t :underline t)))
   `(org-ref-glossary-face ((t :underline t)))
   `(org-ref-acronym-face ((t :underline t)))
;;;;; outline
   `(outline-1 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-orange
                             ,@(when zenbers-scale-outline-headlines
                                 (list :height zenbers-height-plus-4))))))
   `(outline-2 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-green+4
                             ,@(when zenbers-scale-outline-headlines
                                 (list :height zenbers-height-plus-3))))))
   `(outline-3 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-blue-1
                             ,@(when zenbers-scale-outline-headlines
                                 (list :height zenbers-height-plus-2))))))
   `(outline-4 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-yellow-2
                             ,@(when zenbers-scale-outline-headlines
                                 (list :height zenbers-height-plus-1))))))
   `(outline-5 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-cyan))))
   `(outline-6 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-green+2))))
   `(outline-7 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-red-4))))
   `(outline-8 ((t (:inherit ,z-variable-pitch :foreground ,zenbers-blue-4))))
;;;;; p4
   `(p4-depot-added-face ((t :inherit diff-added)))
   `(p4-depot-branch-op-face ((t :inherit diff-changed)))
   `(p4-depot-deleted-face ((t :inherit diff-removed)))
   `(p4-depot-unmapped-face ((t :inherit diff-changed)))
   `(p4-diff-change-face ((t :inherit diff-changed)))
   `(p4-diff-del-face ((t :inherit diff-removed)))
   `(p4-diff-file-face ((t :inherit diff-file-header)))
   `(p4-diff-head-face ((t :inherit diff-header)))
   `(p4-diff-ins-face ((t :inherit diff-added)))
;;;;; c/perl
   `(cperl-nonoverridable-face ((t (:foreground ,zenbers-orange :weight bold))))
   `(cperl-array-face ((t (:foreground ,zenbers-red+1 :backgorund ,zenbers-bg))))
   `(cperl-hash-face ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg))))
   `(cperl-invalid-face ((t (:underline nil))))
;;;;; paren-face
   `(parenthesis ((t (:foreground ,zenbers-fg-1))))
;;;;; perspective
   `(persp-selected-face ((t (:foreground ,zenbers-yellow-2 :inherit mode-line))))
;;;;; pdf-tools
   `(pdf-links-read-link ((t (:foreground ,zenbers-green+2))))
;;;;; powerline
   `(powerline-active0 ((t (:background ,zenbers-bg+2 :inherit mode-line))))
   `(powerline-active1 ((t (:background ,zenbers-bg+1 :inherit mode-line))))
   `(powerline-active2 ((t (:background ,zenbers-bg+05 :inherit mode-line))))
   `(powerline-inactive0 ((t (:background ,zenbers-bg+05))))
   `(powerline-inactive1 ((t (:background ,zenbers-bg+05))))
   `(powerline-inactive2 ((t (:background ,zenbers-bg+05))))
;;;;; proced
   `(proced-cpu ((t (:inherit highlight-numbers-number))))
   `(proced-mem ((t (:inherit highlight-numbers-number))))
   `(proced-user ((t (:foreground ,zenbers-fg-2))))
   `(proced-pid ((t (:foreground ,zenbers-red+1))))
   `(proced-session-leader-pid ((t (:foreground ,zenbers-red+1 :weight bold))))
   `(proced-emacs-pid ((t (:foreground ,zenbers-red-2 :weight bold))))
   `(proced-time-colon ((t (:foreground unspecified))))
   `(proced-executable ((t (:inherit font-lock-function-name-face))))
   `(proced-memory-low-usage ((t (:foreground ,zenbers-green+1))))
   `(proced-memory-medium-usage ((t (:foreground ,zenbers-orange))))
   `(proced-memory-high-usage ((t (:foreground ,zenbers-red))))
   `(proced-ppid ((t (:foreground ,zenbers-blue-2))))
   `(proced-pgrp ((t (:foreground ,zenbers-blue-2))))
   `(proced-sess ((t (:foreground ,zenbers-blue-2))))
   `(proced-run-status-code ((t (:foreground ,zenbers-green))))
   `(proced-interruptible-sleep-status-code ((t (:foreground ,zenbers-fg-2))))
   `(proced-uninterruptible-sleep-status-code ((t (:foreground ,zenbers-red+2))))
   `(proced-sort-header ((t (:foreground ,zenbers-orange :weight bold))))
;;;;; proofgeneral
   `(proof-active-area-face ((t (:underline t))))
   `(proof-boring-face ((t (:foreground ,zenbers-fg :background ,zenbers-bg+2))))
   `(proof-command-mouse-highlight-face ((t (:inherit proof-mouse-highlight-face))))
   `(proof-debug-message-face ((t (:inherit proof-boring-face))))
   `(proof-declaration-name-face ((t (:inherit font-lock-keyword-face :foreground nil))))
   `(proof-eager-annotation-face ((t (:foreground ,zenbers-bg :background ,zenbers-orange))))
   `(proof-error-face ((t (:foreground ,zenbers-fg :background ,zenbers-red-4))))
   `(proof-highlight-dependency-face ((t (:foreground ,zenbers-bg :background ,zenbers-yellow-1))))
   `(proof-highlight-dependent-face ((t (:foreground ,zenbers-bg :background ,zenbers-orange))))
   `(proof-locked-face ((t (:background ,zenbers-blue-5))))
   `(proof-mouse-highlight-face ((t (:foreground ,zenbers-bg :background ,zenbers-orange))))
   `(proof-queue-face ((t (:background ,zenbers-red-4))))
   `(proof-region-mouse-highlight-face ((t (:inherit proof-mouse-highlight-face))))
   `(proof-script-highlight-error-face ((t (:background ,zenbers-red-2))))
   `(proof-tacticals-name-face ((t (:inherit font-lock-constant-face :foreground nil :background ,zenbers-bg))))
   `(proof-tactics-name-face ((t (:inherit font-lock-constant-face :foreground nil :background ,zenbers-bg))))
   `(proof-warning-face ((t (:foreground ,zenbers-bg :background ,zenbers-yellow-1))))
;;;;; quack
   `(quack-pltish-selfeval-face ((t (:foreground ,zenbers-red))))
   `(quack-pltish-paren-face ((t (:foreground ,zenbers-fg-2))))
   `(quack-pltish-comment-face ((t (:foreground ,zenbers-green))))
   `(comint-highlight-prompt ((t (:foreground ,zenbers-green))))
;;;;; q4
   `(q4/id-face ((t (:foreground ,zenbers-red))))
   `(q4/quote-face ((t (:foreground ,zenbers-magenta))))
   `(q4/greentext-face ((t (:foreground ,zenbers-green))))
;;;;; racket-mode
   `(racket-keyword-argument-face ((t (:inherit font-lock-constant-face))))
   `(racket-selfeval-face ((t (:inherit font-lock-type-face))))
;;;;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((t (:foreground ,zenbers-fg))))
   `(rainbow-delimiters-depth-2-face ((t (:foreground ,zenbers-blue-1))))
   `(rainbow-delimiters-depth-3-face ((t (:foreground ,zenbers-green+2))))
   `(rainbow-delimiters-depth-4-face ((t (:foreground ,zenbers-red+2))))
   `(rainbow-delimiters-depth-5-face ((t (:foreground ,zenbers-green+4))))
   `(rainbow-delimiters-depth-6-face ((t (:foreground ,zenbers-magenta))))
   `(rainbow-delimiters-depth-7-face ((t (:foreground ,zenbers-yellow-1))))
   `(rainbow-delimiters-depth-8-face ((t (:foreground ,zenbers-green+1))))
   `(rainbow-delimiters-depth-9-face ((t (:foreground ,zenbers-blue-2))))
   `(rainbow-delimiters-depth-10-face ((t (:foreground ,zenbers-orange))))
   `(rainbow-delimiters-depth-11-face ((t (:foreground ,zenbers-green))))
   `(rainbow-delimiters-depth-12-face ((t (:foreground ,zenbers-blue-5))))
;;;;; rcirc
   `(rcirc-my-nick ((t (:foreground ,zenbers-blue))))
   `(rcirc-other-nick ((t (:foreground ,zenbers-orange))))
   `(rcirc-bright-nick ((t (:foreground ,zenbers-blue+1))))
   `(rcirc-dim-nick ((t (:foreground ,zenbers-blue-2))))
   `(rcirc-server ((t (:foreground ,zenbers-green))))
   `(rcirc-server-prefix ((t (:foreground ,zenbers-green+1))))
   `(rcirc-timestamp ((t (:foreground ,zenbers-green+2))))
   `(rcirc-nick-in-message ((t (:foreground ,zenbers-yellow))))
   `(rcirc-nick-in-message-full-line ((t (:weight bold))))
   `(rcirc-prompt ((t (:foreground ,zenbers-yellow :weight bold))))
   `(rcirc-track-nick ((t (:inverse-video t))))
   `(rcirc-track-keyword ((t (:weight bold))))
   `(rcirc-url ((t (:weight bold))))
   `(rcirc-keyword ((t (:foreground ,zenbers-yellow :weight bold))))
;;;;; re-builder
   `(reb-match-0 ((t (:foreground ,zenbers-bg :background ,zenbers-magenta))))
   `(reb-match-1 ((t (:foreground ,zenbers-bg :background ,zenbers-blue))))
   `(reb-match-2 ((t (:foreground ,zenbers-bg :background ,zenbers-orange))))
   `(reb-match-3 ((t (:foreground ,zenbers-bg :background ,zenbers-red))))
;;;;; realgud
   `(realgud-overlay-arrow1 ((t (:foreground ,zenbers-blue+2))))
   `(realgud-overlay-arrow2 ((t (:foreground ,zenbers-fg-1))))
   `(realgud-overlay-arrow3 ((t (:foreground ,zenbers-bg+3))))
   `(realgud-bp-enabled-face ((t (:foreground ,zenbers-red-2))))
   `(realgud-bp-disabled-face ((t (:foreground ,zenbers-fg-2))))
   `(realgud-bp-line-enabled-face ((t (:box nil))))
   `(realgud-bp-line-disabled-face ((t (:box nil))))
   `(realgud-line-number ((t (:foreground ,zenbers-yellow))))
   `(realgud-backtrace-number ((t (:foreground ,zenbers-yellow :weight bold))))
   `(realgud-debugger-running ((t (:foreground ,zenbers-green :weight bold))))
;;;;; regex-tool
   `(regex-tool-matched-face ((t (:background ,zenbers-blue-4 :weight bold))))
;;;;; rpm-mode
   `(rpm-spec-dir-face ((t (:foreground ,zenbers-green))))
   `(rpm-spec-doc-face ((t (:foreground ,zenbers-green))))
   `(rpm-spec-ghost-face ((t (:foreground ,zenbers-red))))
   `(rpm-spec-macro-face ((t (:foreground ,zenbers-yellow))))
   `(rpm-spec-obsolete-tag-face ((t (:foreground ,zenbers-red))))
   `(rpm-spec-package-face ((t (:foreground ,zenbers-red))))
   `(rpm-spec-section-face ((t (:foreground ,zenbers-yellow))))
   `(rpm-spec-tag-face ((t (:foreground ,zenbers-blue))))
   `(rpm-spec-var-face ((t (:foreground ,zenbers-red))))
;;;;; rst-mode
   `(rst-level-1-face ((t (:foreground ,zenbers-orange))))
   `(rst-level-2-face ((t (:foreground ,zenbers-green+1))))
   `(rst-level-3-face ((t (:foreground ,zenbers-blue-1))))
   `(rst-level-4-face ((t (:foreground ,zenbers-yellow-2))))
   `(rst-level-5-face ((t (:foreground ,zenbers-cyan))))
   `(rst-level-6-face ((t (:foreground ,zenbers-green-2))))
;;;;; ruler-mode
   `(ruler-mode-column-number ((t (:inherit 'ruler-mode-default :foreground ,zenbers-fg))))
   `(ruler-mode-fill-column ((t (:inherit 'ruler-mode-default :foreground ,zenbers-yellow))))
   `(ruler-mode-goal-column ((t (:inherit 'ruler-mode-fill-column))))
   `(ruler-mode-comment-column ((t (:inherit 'ruler-mode-fill-column))))
   `(ruler-mode-tab-stop ((t (:inherit 'ruler-mode-fill-column))))
   `(ruler-mode-current-column ((t (:foreground ,zenbers-yellow :box t))))
   `(ruler-mode-default ((t (:foreground ,zenbers-green+2 :background ,zenbers-bg))))
;;;;; sh-mode
   `(sh-heredoc     ((t (:inherit ,font-lock-string-face))))
   `(sh-quoted-exec ((t (:foreground ,zenbers-red))))
   `(sh-escaped-newline ((t (:foreground ,zenbers-fg-2))))
;;;;; shr
   `(shr-text     ((t (:height 1.0))))
;;;;; show-paren
   `(show-paren-mismatch ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg+3 :weight bold))))
   `(show-paren-match ((t (:foreground ,zenbers-bg :background ,zenbers-fg-2 :weight bold))))
;;;;; slack
   `(slack-message-output-header ((t (:foreground ,zenbers-green :background ,zenbers-bg :weight bold))))
   `(slack-message-output-reaction ((t (:foreground ,zenbers-magenta :background ,zenbers-bg+2))))
   `(slack-preview-face ((t (:foreground ,zenbers-red+2 :background ,zenbers-bg+1))))
;;;;; smart-mode-line
   ;; use (setq sml/theme nil) to enable Zenbers for sml
   `(sml/global ((,class (:foreground ,zenbers-green+1 :weight bold))))
   `(sml/modes ((,class (:foreground ,zenbers-green+1 :weight bold))))
   `(sml/minor-modes ((,class (:foreground ,zenbers-fg-2))))
   `(sml/filename ((,class (:foreground ,zenbers-green+1 :weight bold))))
   `(sml/line-number ((,class (:foreground ,zenbers-green+2))))
   `(sml/col-number ((,class (:foreground ,zenbers-green+2))))
   `(sml/position-percentage ((,class (:foreground ,zenbers-green+1))))
   `(sml/prefix ((,class (:foreground ,zenbers-green+2))))
   `(sml/git ((,class (:foreground ,zenbers-fg-1))))
   `(sml/process ((,class (:weight bold))))
   `(sml/sudo ((,class  (:foreground ,zenbers-blue+2 :weight bold))))
   `(sml/read-only ((,class (:foreground ,zenbers-green+1))))
   `(sml/outside-modified ((,class (:foreground ,zenbers-red))))
   `(sml/modified ((,class (:foreground ,zenbers-red))))
   `(sml/time ((,class (:foreground ,zenbers-green+1))))
   `(sml/vc-edited ((,class (:foreground ,zenbers-green+2))))
   `(sml/charging ((,class (:foreground ,zenbers-green+4))))
   `(sml/discharging ((,class (:foreground ,zenbers-red+1))))
;;;;; smartparens
   `(sp-show-pair-mismatch-face ((t (:foreground ,zenbers-red+1 :background ,zenbers-bg+3 :weight bold))))
   `(sp-show-pair-match-face ((t (:background ,zenbers-bg+3 :weight bold))))
;;;;; sml-mode-line
   '(sml-modeline-end-face ((t :inherit default :width condensed)))
;;;;; SLIME
   `(slime-repl-output-face ((t (:foreground ,zenbers-red))))
   `(slime-repl-inputed-output-face ((t (:foreground ,zenbers-green))))
   `(slime-error-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenbers-red)))
      (t
       (:underline ,zenbers-red))))
   `(slime-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenbers-orange)))
      (t
       (:underline ,zenbers-orange))))
   `(slime-style-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenbers-yellow)))
      (t
       (:underline ,zenbers-yellow))))
   `(slime-note-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenbers-green)))
      (t
       (:underline ,zenbers-green))))
   `(slime-highlight-face ((t (:inherit highlight))))
;;;;; speedbar
   `(speedbar-button-face ((t (:foreground ,zenbers-green+2))))
   `(speedbar-directory-face ((t (:foreground ,zenbers-cyan))))
   `(speedbar-file-face ((t (:foreground ,zenbers-fg))))
   `(speedbar-highlight-face ((t (:foreground ,zenbers-bg :background ,zenbers-green+2))))
   `(speedbar-selected-face ((t (:foreground ,zenbers-red))))
   `(speedbar-separator-face ((t (:foreground ,zenbers-bg :background ,zenbers-blue-1))))
   `(speedbar-tag-face ((t (:foreground ,zenbers-yellow))))
;;;;; sx
   `(sx-custom-button
     ((t (:background ,zenbers-bg+2 :foreground ,zenbers-fg))))
   `(sx-question-list-answers
     ((t (:foreground ,zenbers-green+3
                      :height 1.0 :inherit sx-question-list-parent))))
   `(sx-question-mode-content-face ((t (:background ,zenbers-bg+05))))
   `(sx-question-mode-accepted
     ((t (:foreground ,zenbers-green+3
                      :height 1.3 :inherit sx-question-mode-title))))
   `(sx-question-mode-kbd-tag
     ((t (:box (:color ,zenbers-bg-1 :line-width 3 :style released-button)
               :height 0.9 :weight semi-bold))))
;;;;; tabbar
   `(tabbar-button ((t (:foreground ,zenbers-fg
                                    :background ,zenbers-bg))))
   `(tabbar-selected ((t (:foreground ,zenbers-fg
                                      :background ,zenbers-bg
                                      :box (:line-width -1 :style pressed-button)))))
   `(tabbar-unselected ((t (:foreground ,zenbers-fg
                                        :background ,zenbers-bg+1
                                        :box (:line-width -1 :style released-button)))))
;;;;; term
   `(term-color-black ((t (:foreground ,zenbers-bg))))
   `(term-color-red ((t (:foreground ,zenbers-red-2))))
   `(term-color-green ((t (:foreground ,zenbers-green))))
   `(term-color-yellow ((t (:foreground ,zenbers-yellow))))
   `(term-color-blue ((t (:foreground ,zenbers-blue+2))))
   `(term-color-magenta ((t (:foreground ,zenbers-magenta))))
   `(term-color-cyan ((t (:foreground ,zenbers-cyan))))
   `(term-color-white ((t (:foreground ,zenbers-fg))))
   '(term-default-fg-color ((t (:inherit term-color-white))))
   '(term-default-bg-color ((t (:inherit term-color-black))))
;;;;; undo-tree
   `(undo-tree-visualizer-active-branch-face ((t (:foreground ,zenbers-fg+1 :weight bold))))
   `(undo-tree-visualizer-current-face ((t (:foreground ,zenbers-red-1 :weight bold))))
   `(undo-tree-visualizer-default-face ((t (:foreground ,zenbers-fg-1))))
   `(undo-tree-visualizer-register-face ((t (:foreground ,zenbers-yellow))))
   `(undo-tree-visualizer-unmodified-face ((t (:foreground ,zenbers-orange :weight bold))))
;;;;; vertico
   `(vertico-current ((t (:background ,zenbers-bg+2))))
;;;;; visual-regexp
   `(vr/group-0 ((t (:foreground ,zenbers-bg :background ,zenbers-green :weight bold))))
   `(vr/group-1 ((t (:foreground ,zenbers-bg :background ,zenbers-orange :weight bold))))
   `(vr/group-2 ((t (:foreground ,zenbers-bg :background ,zenbers-blue :weight bold))))
   `(vr/match-0 ((t (:inherit match))))
   `(vr/match-1 ((t (:foreground ,zenbers-yellow-2 :background ,zenbers-bg-1 :weight bold))))
   `(vr/match-separator-face ((t (:foreground ,zenbers-red :weight bold))))
;;;;; volatile-highlights
   `(vhl/default-face ((t (:background ,zenbers-bg-05))))
;;;;; origami-mode
   `(origami-fold-replacement-face ((t (:forground ,zenbers-red :background ,zenbers-bg+1 :weight bold))))
   `(origami-fold-header-face ((t (:forground ,zenbers-green+3 :background ,zenbers-bg+1))))
;;;;; web-mode
   `(web-mode-builtin-face ((t (:inherit ,font-lock-builtin-face))))
   `(web-mode-comment-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-constant-face ((t (:inherit ,font-lock-constant-face))))
   `(web-mode-css-at-rule-face ((t (:foreground ,zenbers-orange ))))
   `(web-mode-css-prop-face ((t (:foreground ,zenbers-orange))))
   `(web-mode-css-property-name-face ((t (:foreground ,zenbers-fg-1))))
   `(web-mode-css-pseudo-class-face ((t (:foreground ,zenbers-red+2 :weight bold))))
   `(web-mode-css-rule-face ((t (:foreground ,zenbers-blue))))
   `(web-mode-css-selector-face ((t (:foreground ,zenbers-orange :weight bold))))
   `(web-mode-doctype-face ((t (:foreground ,zenbers-green+3))))
   `(web-mode-folded-face ((t (:underline t))))
   `(web-mode-function-name-face ((t (:foreground ,zenbers-fg-1))))
   `(web-mode-html-attr-name-face ((t (:foreground ,zenbers-fg-1))))
   `(web-mode-html-attr-value-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-html-tag-face ((t (:foreground ,zenbers-fg-1))))
   `(web-mode-keyword-face ((t (:inherit ,font-lock-keyword-face))))
   `(web-mode-preprocessor-face ((t (:inherit ,font-lock-preprocessor-face))))
   `(web-mode-string-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-type-face ((t (:inherit ,font-lock-type-face))))
   `(web-mode-variable-name-face ((t (:inherit ,font-lock-variable-name-face))))
   `(web-mode-server-background-face ((t (:background ,zenbers-bg))))
   `(web-mode-server-comment-face ((t (:inherit web-mode-comment-face))))
   `(web-mode-server-string-face ((t (:inherit web-mode-string-face))))
   `(web-mode-symbol-face ((t (:inherit font-lock-constant-face))))
   `(web-mode-warning-face ((t (:inherit font-lock-warning-face))))
   `(web-mode-whitespaces-face ((t (:background ,zenbers-red))))
   `(web-mode-block-delimiter-face ((t (:foreground ,zenbers-fg-1 :weight bold))))
;;;;; whitespace-mode
   `(whitespace-space ((t (:foreground ,zenbers-fg-2))))
   `(whitespace-hspace ((t (:foreground ,zenbers-fg-2))))
   `(whitespace-tab ((t (:foreground ,zenbers-fg-2))))
   `(whitespace-newline ((t (:foreground ,zenbers-fg-2))))
   `(whitespace-trailing ((t (:background ,zenbers-red))))
   `(whitespace-line ((t (:foreground ,zenbers-red+2))))
   `(whitespace-space-before-tab ((t (:background ,zenbers-orange :foreground ,zenbers-orange))))
   `(whitespace-indentation ((t (:foreground ,zenbers-fg-2))))
   `(whitespace-empty ((t (:background ,zenbers-yellow))))
   `(whitespace-space-after-tab ((t (:background ,zenbers-yellow :foreground ,zenbers-red))))
;;;;; wanderlust
   `(wl-highlight-folder-few-face ((t (:foreground ,zenbers-red-2))))
   `(wl-highlight-folder-many-face ((t (:foreground ,zenbers-red-1))))
   `(wl-highlight-folder-path-face ((t (:foreground ,zenbers-orange))))
   `(wl-highlight-folder-unread-face ((t (:foreground ,zenbers-blue))))
   `(wl-highlight-folder-zero-face ((t (:foreground ,zenbers-fg))))
   `(wl-highlight-folder-unknown-face ((t (:foreground ,zenbers-blue))))
   `(wl-highlight-message-citation-header ((t (:foreground ,zenbers-red-1))))
   `(wl-highlight-message-cited-text-1 ((t (:foreground ,zenbers-red))))
   `(wl-highlight-message-cited-text-2 ((t (:foreground ,zenbers-green+2))))
   `(wl-highlight-message-cited-text-3 ((t (:foreground ,zenbers-blue))))
   `(wl-highlight-message-cited-text-4 ((t (:foreground ,zenbers-blue+1))))
   `(wl-highlight-message-header-contents-face ((t (:foreground ,zenbers-green))))
   `(wl-highlight-message-headers-face ((t (:foreground ,zenbers-red+1))))
   `(wl-highlight-message-important-header-contents ((t (:foreground ,zenbers-green+2))))
   `(wl-highlight-message-header-contents ((t (:foreground ,zenbers-green+1))))
   `(wl-highlight-message-important-header-contents2 ((t (:foreground ,zenbers-green+2))))
   `(wl-highlight-message-signature ((t (:foreground ,zenbers-green))))
   `(wl-highlight-message-unimportant-header-contents ((t (:foreground ,zenbers-fg))))
   `(wl-highlight-summary-answered-face ((t (:foreground ,zenbers-blue))))
   `(wl-highlight-summary-disposed-face ((t (:foreground ,zenbers-fg
                                                         :slant italic))))
   `(wl-highlight-summary-new-face ((t (:foreground ,zenbers-blue))))
   `(wl-highlight-summary-normal-face ((t (:foreground ,zenbers-fg))))
   `(wl-highlight-summary-thread-top-face ((t (:foreground ,zenbers-yellow))))
   `(wl-highlight-thread-indent-face ((t (:foreground ,zenbers-magenta))))
   `(wl-highlight-summary-refiled-face ((t (:foreground ,zenbers-fg))))
   `(wl-highlight-summary-displaying-face ((t (:underline t :weight bold))))
;;;;; which-func-mode
   `(which-func ((t (:foreground ,zenbers-green+4))))
;;;;; xcscope
   `(cscope-file-face ((t (:foreground ,zenbers-yellow :weight bold))))
   `(cscope-function-face ((t (:foreground ,zenbers-cyan :weight bold))))
   `(cscope-line-number-face ((t (:foreground ,zenbers-red :weight bold))))
   `(cscope-mouse-face ((t (:foreground ,zenbers-bg :background ,zenbers-blue+1))))
   `(cscope-separator-face ((t (:foreground ,zenbers-red :weight bold
                                            :underline t :overline t))))
;;;;; yascroll
   `(yascroll:thumb-text-area ((t (:background ,zenbers-bg-1))))
   `(yascroll:thumb-fringe ((t (:background ,zenbers-bg-1 :foreground ,zenbers-bg-1))))

;;;;; ztree
   `(ztreep-header-face ((t (:foreground ,zenbers-green))))
   `(ztreep-diff-header-face ((t (:inherit ztreep-header-face))))
   `(ztreep-diff-header-small-face ((t (:inherit ztreep-header-face))))
   `(ztreep-diff-model-add-face ((t (:foreground ,zenbers-green+2))))
   `(ztreep-diff-model-diff-face ((t (:foreground ,zenbers-fg))))
   `(ztreep-diff-model-normal-face ((t (:foreground ,zenbers-fg-2))))
   `(ztreep-node-face ((t (:inherit dired-directory))))
   `(ztreep-leaf-face ((t (:inherit diredp-file-name))))
   `(ztreep-arrow-face ((t (:foreground ,zenbers-fg-2))))
   `(ztreep-expand-sign-face ((t (:foreground ,zenbers-fg-2))))))

;;; Theme Variables
(zenbers-with-color-variables
  (custom-theme-set-variables
   'zenbers
;;;;; ansi-color
   `(ansi-color-names-vector [,zenbers-bg ,zenbers-red ,zenbers-green ,zenbers-yellow-2
                                          ,zenbers-blue+2 ,zenbers-magenta ,zenbers-cyan ,zenbers-fg])
;;;;; company-quickhelp
   `(company-quickhelp-color-background ,zenbers-bg+1)
   `(company-quickhelp-color-foreground ,zenbers-fg)
;;;;; fill-column-indicator
   `(fci-rule-color ,zenbers-bg-05)
;;;;; nrepl-client
   `(nrepl-message-colors
     '(,zenbers-red ,zenbers-orange ,zenbers-yellow ,zenbers-green ,zenbers-green+4
                    ,zenbers-cyan ,zenbers-blue+1 ,zenbers-magenta))
;;;;; pdf-tools
   `(pdf-view-midnight-colors '("#D9D9D9" . "#1C1C1C"))
;;;;; rustic
   `(rustic-ansi-faces [,zenbers-bg ,zenbers-red ,zenbers-green ,zenbers-blue+1
                                    ,zenbers-fg-2 ,zenbers-magenta ,zenbers-cyan ,zenbers-fg])
;;;;; vc-annotate
   `(vc-annotate-color-map
     '(( 20. . ,zenbers-red-1)
       ( 40. . ,zenbers-red)
       ( 60. . ,zenbers-orange)
       ( 80. . ,zenbers-yellow-2)
       (100. . ,zenbers-yellow-1)
       (120. . ,zenbers-yellow)
       (140. . ,zenbers-green-2)
       (160. . ,zenbers-green)
       (180. . ,zenbers-green+1)
       (200. . ,zenbers-green+2)
       (220. . ,zenbers-green+3)
       (240. . ,zenbers-green+4)
       (260. . ,zenbers-cyan)
       (280. . ,zenbers-blue-2)
       (300. . ,zenbers-blue-1)
       (320. . ,zenbers-blue)
       (340. . ,zenbers-blue+1)
       (360. . ,zenbers-magenta)))
   `(vc-annotate-very-old-color ,zenbers-magenta)
   `(vc-annotate-background ,zenbers-bg-1)
;;;;; xterm
   `(xterm-color-names [,zenbers-fg-2 ,zenbers-red ,zenbers-green ,zenbers-yellow
                                      ,zenbers-blue+2 ,zenbers-magenta ,zenbers-cyan ,zenbers-fg])
   `(xterm-color-names-bright [,zenbers-bg+3 ,zenbers-red ,zenbers-green+1 ,zenbers-yellow
                                             ,zenbers-blue+2 ,zenbers-magenta ,zenbers-cyan ,zenbers-fg])

;;;;; vterm
   `(vterm-color-black ((t (:inherit term-color-black))))
   `(vterm-color-red ((t (:inherit term-color-red))))
   `(vterm-color-green ((t (:inherit term-color-green))))
   `(vterm-color-yellow ((t (:inherit term-color-yellow))))
   `(vterm-color-blue ((t (:inherit term-color-blue))))
   `(vterm-color-magenta ((t (:inherit term-color-magenta))))
   `(vterm-color-cyan ((t (:inherit term-color-cyan))))
   `(vterm-color-white ((t (:inherit term-color-white))))
   `(vterm-color-default ((t (:inherit term-color-white))))
   ))

;;; Rainbow Support

(declare-function rainbow-mode 'rainbow-mode)
(declare-function rainbow-colorize-by-assoc 'rainbow-mode)

(defvar zenbers-add-font-lock-keywords nil
  "Whether to add font-lock keywords for zenbers color names.
In buffers visiting library `zenbers-theme.el' the zenbers
specific keywords are always added.  In all other Emacs-Lisp
buffers this variable controls whether this should be done.
This requires library `rainbow-mode'.")

(defvar zenbers-colors-font-lock-keywords nil)

;; (defadvice rainbow-turn-on (after zenbers activate)
;;   "Maybe also add font-lock keywords for zenbers colors."
;;   (when (and (derived-mode-p 'emacs-lisp-mode)
;;              (or zenbers-add-font-lock-keywords
;;                  (equal (file-name-nondirectory (buffer-file-name))
;;                         "zenbers-theme.el")))
;;     (unless zenbers-colors-font-lock-keywords
;;       (setq zenbers-colors-font-lock-keywords
;;             `((,(regexp-opt (mapcar 'car zenbers-colors-alist) 'words)
;;                (0 (rainbow-colorize-by-assoc zenbers-colors-alist))))))
;;     (font-lock-add-keywords nil zenbers-colors-font-lock-keywords)))

;; (defadvice rainbow-turn-off (after zenbers activate)
;;   "Also remove font-lock keywords for zenbers colors."
;;   (font-lock-remove-keywords nil zenbers-colors-font-lock-keywords))

;;; Footer

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'zenbers)

;; Local Variables:
;; no-byte-compile: t
;; indent-tabs-mode: nil
;; eval: (when (require 'rainbow-mode nil t) (rainbow-mode 1))
;; End:
;;; zenbers-theme.el ends here

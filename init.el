(defun /user-path (path)
  "Absolute of `PATH' with root directory as `user-emacs-directory'"
  (expand-file-name path user-emacs-directory))

(defun /load (path &rest args)
  "Pipe to `load' with `PATH' root as `user-emacs-directory'"
  (load (/user-path path) args))

(/load "config.el")
(/load "bespoke.el")
(/load "hacks.el")
(/load ".secret.el" t)
(/load "custom-file.el" t)

;;; Completion
(marginalia-mode t)
(vertico-mode t)
(recentf-mode t)

;;; Interface
(server-start)
(eyebrowse-mode t)
(winner-mode t)
(shackle-mode t)
(openwith-mode t)
(global-auto-revert-mode t)

;;; Editing
(indent-tabs-mode nil)
(global-eldoc-mode t)
(global-undo-tree-mode t)
(yas-global-mode t)
(show-paren-mode t)
(global-so-long-mode t)
(save-place-mode t)
(projectile-mode -1) ; Performance issues

;;; Shell
(coterm-mode t)
(shx-global-mode t)
(global-fish-completion-mode t)

;;; Display
(global-page-break-lines-mode t)
(blink-cursor-mode t)
(fortune-cookie-mode t)
(pdf-tools-install t)

;; Mode-line
(sml/setup)
(column-number-mode t)
(line-number-mode t)
(size-indication-mode t)
(fancy-battery-mode -1) ; Enable when on laptop

;; Comms
(ednc-mode t)

(provide 'init.el)

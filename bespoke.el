(defun /print-current-date-time ()
  "Print the current date and time to the echo area"
  (interactive)
  (message (format-time-string "%Y-%m-%d %H:%M:%S")))
(global-set-key (kbd "C-c n") '/print-current-date-time)



(defconst shell-modes-list '(shell-mode vterm-mode eshell-mode term-mode))

(defun /new-shell-buffer (buf)
  "Create a new shell buffer, inferring the major-mode by BUFNAME or prompting for it"
  (let* ((inferred-mode (concat (car (split-string buf "-" nil "^\\*")) "-mode"))
         (actual-mode (if (cl-member (intern inferred-mode) shell-modes-list) inferred-mode
                        (completing-read "mode: " shell-modes-list nil t)))
         (init-func (string-trim actual-mode nil "-mode")))
    (funcall (intern init-func) buf)))

(defun /shell-like-buffer-p (buf)
  "Returns t if BUF is a shell-like buffer"
  (or (cl-member (buffer-local-value 'major-mode buf) shell-modes-list)
      ;; Also include X window terminals
      (and (string-equal (buffer-local-value 'major-mode buf) "exwm-mode")
           (cl-some (lambda (term-name)
                      (string-match-p term-name (buffer-name buf)))
                    (list "urxvt" "xterm")))))

(defun /switch-to-shell-buffer ()
  "Switch to an existing shell buffer or create a new one"
  (interactive)
  (let* ((shell-buffers (cl-remove-if-not '/shell-like-buffer-p (buffer-list)))
         (buf (completing-read "shell: " (mapcar 'buffer-name shell-buffers) nil nil)))
    (if (get-buffer buf)
        (switch-to-buffer buf)
      (/new-shell-buffer buf))))
(global-set-key (kbd "C-c s") '/switch-to-shell-buffer)



(define-minor-mode /visual-reading-mode
  "Visual-fill text without splitting words"
  :global nil
  :keymap nil
  :lighter " Reading"
  (use-package visual-fill-column :straight t)
  (let ((enable (if /visual-reading-mode 1 0)))
    (visual-fill-column-mode enable)
    (visual-line-mode enable)))



(defun /media-setup ()
  "Set up media workspace"
  (interactive)
  (eyebrowse-create-window-config)
  (eyebrowse-rename-window-config  (eyebrowse--get 'current-slot)  "media")
  (delete-other-windows)
  (emms-smart-browse)
  (transpose-frame)
  (split-window-below)
  (elfeed)
  (split-window-below)
  (transmission)
  (balance-windows))

(defun /org-agenda-setup ()
  "Set org agenda workspace"
  (interactive)
  (eyebrowse-create-window-config)
  (eyebrowse-rename-window-config  (eyebrowse--get 'current-slot)  "org")
  (delete-other-windows)
  (org-agenda-list)
  (split-window-below)
  (find-file (/org-agenda-path "agenda.org"))
  (org-cycle-content)
  (split-window-below)
  (find-file (/org-agenda-path "project.org"))
  (org-cycle-content)
  (balance-windows))



;; A useful macro for configuring connections to IRC servers. Invoked in my secrets file.
(defmacro /erc-auto-connect (command server port nick password buffername)
  "Create interactive command `COMMAND' for connecting to an IRC
server, using `SERVER', `PORT', `NICK', and `PASSWORD' as
arguments. The connection buffer will be named `BUFFERNAME'. The
command uses interactive mode if passed an argument."
  (fset command `(lambda (arg)
                   (interactive "p")
                   (if (not (= 1 arg))
                       (call-interactively 'erc)
                     (erc :server ,server :port ,port :nick ,nick :password ,password)
                     (rename-buffer ,buffername)))))



(defun /mpv (target)
  "Start an mpv process targeting TARGET"
  (start-process-shell-command
   "mpv" nil (format "mpv '%s'" target)))

(defun /mpv-kill-ring ()
  "Start mpv targeting the killring's contents"
  (interactive)
  (/mpv (substring-no-properties (current-kill 0))))
(global-set-key (kbd "C-c o") '/mpv-kill-ring)



(defconst path-to-ctags (executable-find "ctags"))

(let ((my-tags-file (locate-dominating-file default-directory "TAGS")))
  (when my-tags-file
    (message "Loading tags file: %s" my-tags-file)
    (visit-tags-table my-tags-file)))

(defun /create-tags (dir-name)
  "Create tags file within DIR-NAME."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f TAGS -e -R %s" path-to-ctags (directory-file-name dir-name))))

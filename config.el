;;; Setup and early-init

(use-package straight
  :init
  (defun /bootstrap-straight ()
    "Bootstrap straight if not installed"
    (defvar bootstrap-version)
    (let ((url "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el")
          (bootstrap-version 7)
          (bootstrap-file (expand-file-name "straight/repos/straight.el/bootstrap.el"
                                            (or (bound-and-true-p straight-base-dir)
                                                user-emacs-directory))))
      (unless (file-exists-p bootstrap-file)
        (with-current-buffer (url-retrieve-synchronously url 'silent 'inhibit-cookies)
          (goto-char (point-max))
          (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage)))
  (/bootstrap-straight)

  :custom
  (package-enable-at-startup nil)) ; Also set in early-init.el

(use-package no-littering :straight t)

(use-package use-package
  :custom (use-package-hook-name-suffix nil)
  :config
  (use-package use-package-ensure-system-package
    :init (use-package system-packages :straight t)
    :custom (system-packages-use-sudo t))
  (use-package diminish :straight t))


;;; General

(use-package emacs
  :custom
  ;; Open new windows by splitting vertically
  (split-height-threshold nil)
  (split-width-threshold 160)

  ;; Don't show the cursor in non-selected windows.
  (cursor-in-non-selected-windows nil)

  ;; Don't show any dialog boxes or help messages
  (show-help-function nil)
  (use-dialog-box nil)

  ;; Don't warn me about opening large files, it's likely the file is
  ;; video or audio and will be opened externally anyway.
  (large-file-warning-threshold nil)

  ;; Sync emacs clipboard with X
  (select-enable-clipboard t)
  (select-enable-primary t)

  ;; Scrolling
  (scroll-conservatively 1) ; Only scroll by 1 line when cursor goes out of view.
  (scroll-step 1)
  (auto-hscroll-mode 'current-line)
  (truncate-lines t)

  ;; Display
  (x-pointer-shape x-pointer-xterm)
  (line-spacing 0)

  ;; Completion
  (completion-ignore-case t)
  (completion-ignored-extensions nil)

  ;; Miscellaneous
  (fill-column 72)
  (tab-width 8)
  (lisp-body-indent 2)
  (inhibit-splash-screen t)
  (sentence-end-double-space nil)
  (make-backup-files nil)
  (package-native-compile t)
  (ring-bell-function 'ignore)
  (bookmark-fringe-mark nil)
  (indent-tabs-mode nil)
  (warning-minimum-level :error)
  (display-raw-bytes-as-hex t)

  (custom-file (/user-path "custom-file.el"))

  :bind (;; Useful for development
         ("C-c m" . recompile)
         ("C-c M" . switch-to-compilation-buffer)
         ("<C-i>" . completion-at-point) ; Easier than C-M-i

         ;; Quick keys for resizing windows
         ("<C-up>" . enlarge-window)
         ("<C-down>" . shrink-window)
         ("<C-left>" . shrink-window-horizontally)
         ("<C-right>" . enlarge-window-horizontally)
         ("C-c t b" . fit-window-to-buffer)

         ;; Use regexp search by default
         ("C-s" . isearch-forward-regexp)
         ("C-r" . isearch-backward-regexp)
         ("C-M-s" . isearch-forward)
         ("C-M-r" . isearch-backward)
         ("M-%" . query-replace-regexp)
         ("C-M-%" . query-replace)

         ;; Use dwim for case functions
         ("M-c" . capitalize-dwim)
         ("M-l" . downcase-dwim)
         ("M-u" . upcase-dwim)

         ;; Prevent nasty typos
         ("C-z" . repeat) ; C-z freezes the frame - remap this to repeat
         ("C-x C-z" . ignore) ; Usually suspends emacs
         ("C-x 5 0" . ignore) ; These usually deletes frames
         ("C-x 5 1" . ignore)
         ("<XF86Mail>" . ignore) ; I use this for push-to-talk in other programs

         ;; Control profiler without using M-x
         ("C-c z p p" . profiler-start)
         ("C-c z p s" . profiler-stop)
         ("C-c z p r" . profiler-report)

         ;; Inspection
         ("C-c d f" . describe-face)
         ("C-c y" . revert-buffer))

  :config
  (defun switch-to-compilation-buffer ()
    "Switch to the compilation buffer"
    (interactive)
    (switch-to-buffer next-error-last-buffer))

  ;; Set my font
  (add-to-list 'default-frame-alist '(font . "Terminus-11"))
  (set-frame-font "Terminus-11" nil t)

  ;; Set my theme
  (/load "zenbers-theme.el")
  (load-theme 'zenbers t)

  ;; Disable GUI elements
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1)
  (fringe-mode '(0 . 9)) ; Right fringe only

  ;; Use y/n instead of yes/no
  (fset 'yes-or-no-p 'y-or-n-p)

  ;; Undisable functions
  (put 'scroll-left 'disabled nil)
  (put 'narrow-to-region 'disabled nil)

  ;; Enabled commands
  (put 'erase-buffer 'disabled nil)

  ;; Free up some extra keys that are usually disabled because they
  ;; collide with VT100 keys.
  (when (display-graphic-p)
    (define-key input-decode-map [?\C-i] [C-i])
    (define-key input-decode-map [?\C-m] [C-m])))

(use-package exwm :straight t :demand t
  :hook ((exwm-update-class-hook . /exwm-set-x-buffer-name))
  :config
  (defun /buffers-with-mode (mode)
    "Return names of buffers with MODE as their `major-mode'."
    (cl-map 'list 'buffer-name
            (cl-remove-if-not
             (lambda (buf)
               (eq (buffer-local-value 'major-mode buf) mode))
             (buffer-list))))

  (defun /switch-to-exwm-buffer ()
    "Switch to an X Windows buffer"
    (interactive)
    (switch-to-buffer
     (completing-read "X Window: " (/buffers-with-mode 'exwm-mode) nil t)))
  (global-set-key (kbd "C-c x") '/switch-to-exwm-buffer)

  (defun /exwm-shell-command (command)
    "Execute COMMAND asynchronously. Intended as a launcher for external programs"
    (interactive (list (read-shell-command "$ ")))
    (start-process-shell-command command nil command))

  (defun /exwm-set-x-buffer-name ()
    "Set EXWM buffer name"
    (exwm-workspace-rename-buffer
     (format "*%s*" (downcase exwm-class-name))))

  (defun /glitchlock ()
    "Execute shell command `glitchlock` -- my custom lockscreen"
    (interactive)
    (shell-command "glitchlock"))

  (setq exwm-floating-border-color "#2F2F2F")
  (setq exwm-floating-border-width 3)
  (setq exwm-layout-show-all-buffers t)
  (setq exwm-workspace-show-all-buffers t)
  (setq exwm-workspace-warp-cursor t)

  ;; I also load a machine-specific configuration if it exists. An example of
  ;; its contents can be found below:
  ;;
  ;; ;; EXWM Monitor Configuration
  ;; (require 'exwm-randr)
  ;; (setq exwm-workspace-number 2)
  ;; (setq exwm-workspace-switch-create-limit 2)
  ;; (setq exwm-randr-workspace-monitor-plist '(0 "DVI-I-1" 1 "DVI-D-0"))
  ;;
  ;; (exwm-input-set-key (kbd "s-e") #'(lambda () (interactive) (exwm-workspace-switch-create 0)))
  ;; (exwm-input-set-key (kbd "s-n") #'(lambda () (interactive) (exwm-workspace-switch-create 1)))
  ;;
  ;; (exwm-randr-mode 1)
  (/load "exwm-config.el" t)
  (exwm-enable)

  (exwm-input-set-key (kbd "s-r") 'exwm-reset)
  (exwm-input-set-key (kbd "s-o") 'other-window)
  (exwm-input-set-key (kbd "s-u") 'split-window-below)
  (exwm-input-set-key (kbd "s-y") 'split-window-right)
  (exwm-input-set-key (kbd "s-k") 'delete-window)
  (exwm-input-set-key (kbd "s-m") 'delete-other-windows)
  (exwm-input-set-key (kbd "s-l") '/glitchlock)
  (exwm-input-set-key (kbd "s-<return>") '/exwm-shell-command)

  (exwm-input-set-simulation-key (kbd "C-b") (kbd "<left>"))
  (exwm-input-set-simulation-key (kbd "C-f") (kbd "<right>"))
  (exwm-input-set-simulation-key (kbd "C-p") (kbd "<up>"))
  (exwm-input-set-simulation-key (kbd "C-n") (kbd "<down>"))
  (exwm-input-set-simulation-key (kbd "C-a") (kbd "<home>"))
  (exwm-input-set-simulation-key (kbd "C-e") (kbd "<end>"))
  (exwm-input-set-simulation-key (kbd "M-v") (kbd "<prior>"))
  (exwm-input-set-simulation-key (kbd "C-v") (kbd "<next>"))
  (exwm-input-set-simulation-key (kbd "C-d") (kbd "<delete>"))
  (exwm-input-set-simulation-key (kbd "C-g") (kbd "<escape>"))
  (exwm-input-set-simulation-key (kbd "M-w") (kbd "C-c"))
  (exwm-input-set-simulation-key (kbd "C-y") (kbd "C-v"))
  (exwm-input-set-simulation-key (kbd "C-s") (kbd "C-g"))
  (exwm-input-set-simulation-key (kbd "C-r") (kbd "C-S-g"))
  (exwm-input-set-simulation-key (kbd "C-k") [S-end delete]))


;;; Completion

(use-package completion-preview :diminish
  :hook ((prog-mode-hook . completion-preview-mode)
         (comint-mode-hook . completion-preview-mode)
         (shell-mode-hook . (lambda () (setq-local pcomplete-termination-string ""))))
  :bind (:map completion-preview-active-mode-map
              ("M-i" . completion-preview-complete)
              ("M-n" . completion-preview-next-candidate)
              ("M-p" . completion-preview-prev-candidate)))

(use-package marginalia :straight t)

(use-package orderless :straight t
  :config (setq completion-styles '(orderless partial-completion basic)))

(use-package vertico :straight t :diminish
  :init (ido-mode -1)
  :bind (:map vertico-map ("<C-i>" . vertico-insert))
  :config
  ;; Use grid-layout for jinx to fit more suggestions on screen
  (add-to-list 'vertico-multiform-categories '(jinx grid))
  (vertico-multiform-mode 1))

(use-package consult :straight t :demand t
  :init
  (use-package org)
  (use-package shell)
  (use-package markdown-mode)
  :after (:any shell org markdown-mode)
  :custom
  ;; Use consult as my completion-in-region
  (completion-in-region-function #'consult-completion-in-region)

  (consult-goto-line-numbers nil)

  ;; Consult previews don't play nice with openwith or exwm
  (consult-preview-excluded-files '(".mp4$" ".mkv$" ".avi$" ".webm$" ".m2ts$"))
  (consult-preview-excluded-buffers '/exwm-buffer-p)

  :bind (("M-s o" . consult-line)
         ("M-g i" . consult-imenu)
         ("M-g M-g" . consult-goto-line)
         ("C-x r b" . consult-bookmark)
         ("M-y" . consult-yank-pop)
         ("C-x C-b" . consult-buffer)
         ("C-c j" . consult-git-grep)
         ("C-c k" . consult-ripgrep)
         ("C-c f" . consult-fd)
         :map minibuffer-mode-map
         ("M-r" . consult-history)
         :map org-mode-map
         ("C-c C-j" . consult-org-heading)
         ("C-c a h" . consult-org-agenda)
         :map markdown-mode-map
         ("C-c C-j" . /consult-md-heading)
         :map shell-mode-map
         ("M-r" . consult-history)
         :map emacs-lisp-mode-map
         ("C-c C-j" . /consult-elisp-heading))
  :config
  (use-package embark-consult :straight t)

  (defun /consult-elisp-heading ()
    "Jump to an Elisp heading"
    (interactive)
    (consult-line "^;;; "))

  (defun /consult-md-heading ()
    "Jump to a markdown heading"
    (interactive)
    (consult-line "^# ")))

(defun /exwm-buffer-p (buf)
  (eq (buffer-local-value 'major-mode buf) 'exwm-mode))

(defun /new-terminal-here (shell-type path)
  "Create a new shell or vterm here"
  (let ((default-directory (file-name-directory path))
        (directory-name (file-name-nondirectory
                         (directory-file-name
                          (file-name-directory path)))))
    (funcall shell-type (format "*%s-%s*" shell-type directory-name))))
(defun /new-shell-here (path) "Create a new shell buffer" (/new-terminal-here 'shell path))
(defun /new-vterm-here (path) "Create a new vterm buffer" (/new-terminal-here 'vterm path))

(use-package embark :straight t
  :custom
  (embark-prompter 'embark-completing-read-prompter)
  (embark-indicators '(embark-minimal-indicator
                       embark-highlight-indicator
                       embark-isearch-highlight-indicator))
  :bind (("C-." . embark-act)
         ("C-h b" . embark-bindings)
         :map embark-file-map
         ("l" . make-symbolic-link)
         ("s" . /new-shell-here)
         ("v" . /new-vterm-here)))

(use-package corfu :straight t :disabled
  :custom
  ;; I find the dynamic width of corfu's frame to be a bit
  ;; distracting,so I set it to a constant width.
  (corfu-min-width 100)
  (corfu-max-width 100))


;;; Editing

(use-package eglot :demand t
  :custom
  (eglot-ignored-server-capabilities '(:documentHighlightProvider)) ; Disable symbol highlighting
  (eglot-send-changes-idle-time 0) ; Stop buffer getting out of sync with server
  :bind (("C-c r" . eglot-rename))
  :hook ((eglot-managed-mode-hook . (lambda () (eglot-inlay-hints-mode 0)))
         (zig-mode-hook . eglot-ensure)
         (rust-mode-hook . eglot-ensure)))

(use-package xref
  :custom
  (xref-show-definitions-function #'xref-show-definitions-completing-read)
  (xref-show-xrefs-function #'xref-show-definitions-completing-read))

(use-package eldoc :diminish
  :custom (eldoc-display-functions '(eldoc-display-in-buffer))
  :bind (("C-c i" . eldoc-print-current-symbol-info)))

(use-package flymake :diminish
  :custom (flymake-no-changes-timeout nil))

(use-package jinx :straight t
  :ensure-system-package (("/usr/include/enchant-2/enchant.h" . enchant)
                          ("/usr/share/hunspell/en_US.aff" . hunspell-en_us)
                          ("/usr/share/hunspell/en_GB.aff" . hunspell-en_gb))
  :hook (git-commit-setup-hook markdown-mode-hook mu4e-compose-mode-hook))

(use-package dumb-jump :straight t :demand t
  :hook (xref-backend-functions . dumb-jump-xref-activate))

(use-package undo-tree :straight t :diminish
  :custom
  (undo-tree-enable-undo-in-region nil)
  (undo-tree-auto-save-history nil))

(use-package abbrev :diminish)

(use-package ediff :defer t
  :custom
  (ediff-window-setup-function 'ediff-setup-windows-plain)
  (ediff-merge-split-window-function 'split-window-horizontally)
  (ediff-split-window-function 'split-window-horizontally))

(use-package ztree :straight t :defer t
  :defer t
  :custom (ztree-draw-unicode-lines t))

(use-package magit :straight t :defer t
  :custom (magit-bury-buffer-function 'magit-restore-window-configuration)
  :config
  ;; Don't update revision buffer every time I move my cursor as it can get too slow.
  (remove-hook 'magit-blame-goto-chunk-hook 'magit-blame-maybe-update-revision-buffer))

(use-package vc
  :custom (vc-follow-symlinks t))

(use-package smerge-mode :defer t :diminish)

(use-package yasnippet :straight t :defer t
  :diminish yas-minor-mode
  :hook (after-init-hook . /yas-cleanup)
  :config
  (defun /yas-cleanup ()
    "Clean up a directory that yas creates on init."
    (delete-directory (expand-file-name "snippets" user-emacs-directory) t))

  (use-package yasnippet-snippets :straight t)
  (yas-reload-all))

(use-package format-all :straight t :defer t
  :bind (("C-c l" . format-all-buffer)))

(use-package subword :diminish
  :hook (prog-mode-hook . subword-mode))

(use-package autorevert
  :custom (global-auto-revert-non-file-buffers t))

(use-package expand-region :straight t
  :bind (("C-;" . 'er/expand-region)))

(use-package unfill :straight t)

(use-package semantic
  :custom
  ;; Stop annoying messages in the minibuffer
  (semantic-minimum-working-buffer-size 20000))

(use-package define-word :straight t)

(use-package string-inflection :straight t
  :bind (("C-c c -" . string-inflection-kebab-case)
         ("C-c c _" . string-inflection-underscore)
         ("C-c c U" . string-inflection-upcase)
         ("C-c c c" . string-inflection-camelcase)
         ("C-c c l" . string-inflection-lower-camelcase)))


;;; Debugging

(use-package realgud :straight t :defer t
  :custom
  (realgud-bp-use-fringe t)
  (realgud-bp-fringe-indicator-style '(realgud-bp-filled . realgud-bp-hollow)))

(use-package gdb-mi :defer t
  :config
  (defun /breakpoint-icon (hexstring)
    "Get breakpoint icon with HEXSTRING as color"
    (list 'image :type 'xpm
          :ascent 100
          :pointer 'hand
          :data
          (format "/* XPM */
static char *magick[] = {
/* columns rows colors chars-per-pixel */
\"10 10 2 1\",
\"  c #%s\",
\"+ c None\",
/* pixels */
\"+++    +++\",
\"++++    ++\",
\"+++++    +\",
\" +++++    \",
\"  +++++   \",
\"   +++++  \",
\"    +++++ \",
\"+    +++++\",
\"++    ++++\",
\"+++    +++\",
};" hexstring)))

  :custom
  (breakpoint-enabled-icon (/breakpoint-icon "D78787"))
  (breakpoint-disabled-icon (/breakpoint-icon "888888")))


;;; Interface

(use-package eyebrowse :straight t
  :hook ((exwm-init-hook . /eyebrowse-workspaces-init)) ;XXX
  :custom
  (eyebrowse-mode-line-style 'current)
  (eyebrowse-tagged-slot-format "%t") ; I only use tags for my workspaces, not numbers
  :config
  (defun /eyebrowse-workspaces-init ()
    "Initialize all eyebrowse workspaces"
    (dolist (frame (frame-list))
      (select-frame frame)
      (eyebrowse-rename-window-config (eyebrowse--get 'current-slot frame) "scratch")))

  (when (featurep 'exwm)
    (exwm-input-set-key (kbd "s-.") #'eyebrowse-create-named-window-config)
    (exwm-input-set-key (kbd "s-,") #'eyebrowse-rename-window-config)
    (exwm-input-set-key (kbd "s-'") #'eyebrowse-close-window-config)
    (exwm-input-set-key (kbd "s-SPC") #'eyebrowse-switch-to-window-config)))

(use-package projectile :straight t :diminish
  :bind (:map projectile-mode-map
              ("C-x p" . projectile-command-map))

  :config
  (defun /projectile-cleanup ()
    "Clean up a file that projectile creates on init. Its recreated and used in the correct place afterwards"
    (delete-file (expand-file-name "projectile-bookmarks.eld" user-emacs-directory)))

  :hook (after-init-hook . /projectile-cleanup))

(use-package ibuffer :straight t
  :custom
  (ibuffer-expert t)
  (ibuffer-marked-face 'diredp-flag-mark)
  (ibuffer-title-face 'font-lock-comment-face))

(use-package transpose-frame :straight t
  :bind (("C-c t i" . flip-frame)
         ("C-c t o" . flop-frame)
         ("C-c t r" . rotate-frame)
         ("C-c t t" . transpose-frame)))

(use-package ace-jump-mode :straight t :demand t
  :custom (ace-jump-mode-scope 'frame)
  :bind ("<C-m>" . ace-jump-mode))

(use-package ace-window :straight t
  :bind ("M-o" . ace-window)
  :custom
  (aw-scope 'global)
  (aw-background t)
  (aw-ignore-current nil)         ; I may want aw to perform an
  (aw-dispatch-when-more-than 2)  ; action on the current window
  (aw-keys '(?n ?e ?i ?h ?l ?y ?k ?N ?E ?I ?H ?L ?Y
                ?j ?p ?f ?a ?d ?g ?i ?l ?r ?s ?t ?y)))

(use-package windmove :straight t :demand t
  :bind (("S-<up>" . windmove-up)
         ("S-<down>" . windmove-down)
         ("S-<left>" . windmove-left)
         ("S-<right>" . windmove-right))
  :config
  (when (featurep 'exwm)
    (exwm-input-set-key (kbd "s-<up>") 'windmove-up)
    (exwm-input-set-key (kbd "s-<down>") 'windmove-down)
    (exwm-input-set-key (kbd "s-<left>") 'windmove-left)
    (exwm-input-set-key (kbd "s-<right>") 'windmove-right)))

(use-package shackle :straight t
  :custom
  (shackle-rules '(("\\`\\*helm.*\\*\\'" :regexp t :align t :ratio 0.4)
                   ("\\`\\*shell.*\\*\\'" :regexp t :same t)
                   ("\\`\\*Man.*\\*\\'" :regexp t :same t)
                   ("\\`\\*gud-.*\\*\\'" :regexp t :same t)
                   ("\\`\\*ChatGPT.*\\*\\'" :regexp t :same t)
                   ("\\`\\*hackernews.*\\*\\'" :regexp t :same t)
                   ("magit: .*" :regexp t :same t)
                   ("*transmission*" :same t)
                   ("*Proced*" :same t)
                   ("*haskell*" :same t)
                   ("*Help*" :same t)
                   ("*compilation*" :ignore t))))

(use-package winner
  :config
  (when (featurep 'exwm)
    (exwm-input-set-key (kbd "s-/") #'winner-undo)
    (exwm-input-set-key (kbd "s-_") #'winner-redo)))

(use-package recentf
  :config
  (when (featurep 'no-littering)
    (add-to-list 'recentf-exclude (recentf-expand-file-name no-littering-var-directory))
    (add-to-list 'recentf-exclude (recentf-expand-file-name no-littering-etc-directory))))

(use-package calc
  :custom (calc-multiplication-has-precedence nil))


;;; Languages

(use-package elisp-mode
  :hook ((emacs-lisp-mode-hook . prettify-symbols-mode)))

(use-package cc-mode
  :custom
  (c-default-style "k&r")
  (c-basic-offset 4)
  (comment-style 'multi-line)
  :config
  (defun /linux-indent-setup ()
    "Set up current buffer for Linux-style indentation"
    (interactive)
    (setq-local c-basic-offset 8)
    (indent-tabs-mode t)))

(use-package zig-mode :straight t :defer t
  :init (use-package reformatter)
  :custom (zig-format-on-save nil)
  :bind (:map zig-mode-map ("C-c C-f" . nil)))

(use-package rust-mode :straight t :defer t
  :hook (rust-mode-hook . /rust-highlight-true-false)
  :config
  (defun /rust-highlight-true-false ()
    "Reface true/false literals as font-lock-constant-face"
    (highlight-regexp "\\b\\(true\\|false\\)\\b" font-lock-constant-face)))

(use-package go-mode :straight t :defer t)

(use-package python :defer t)

(use-package haskell-mode :straight t :defer t
  :custom
  (haskell-indent-offset 4)
  (haskell-process-type 'cabal-repl)
  (haskell-process-show-debug-tips nil)
  (haskell-interactive-popup-errors nil)
  (haskell-stylish-on-save nil)
  (haskell-interactive-mode-read-only nil)

  :hook ((haskell-mode-hook . interactive-haskell-mode)
         (haskell-mode-hook . haskell-indent-mode))

  :bind (:map haskell-interactive-mode-map
              ("C-c M-o" . haskell-interactive-mode-clear))
  :config
  (use-package haskell-interactive-mode))

(use-package markdown-mode :straight t :defer t
  :custom (markdown-hide-urls t))

(use-package css-mode :straight t :defer t
  :custom (css-fontify-colors t))

(use-package web-mode :straight t :defer t
  :hook php-mode-hook
  :custom (web-mode-enable-auto-indentation nil))

(use-package meson-mode :straight t :defer t)

(use-package dts-mode :straight t :defer t)

(use-package kconfig-mode :straight t :defer t)

(use-package crontab-mode :straight t :defer t)

(use-package glsl-mode :straight t :defer t
  :mode ("\\.vs" "\\.fs"))

(use-package nix-mode :straight t :defer t)

(use-package yaml-mode :straight t :defer t)

(use-package dockerfile-mode :straight t :defer t)

(use-package bitbake :straight t :defer t
  :mode ("\\.\\(bb\\(append\\|class\\)?\\|inc\\)\\'" . bitbake-mode))

(use-package qt-pro-mode :straight t :defer t
  :mode ("\\.pro"))

(use-package tidal :straight t :defer t
  :bind (:map tidal-mode-map ("C-v" . nil)))

(use-package groovy-mode :straight t :defer t)

(use-package ledger-mode :straight t :defer t
  :ensure-system-package ledger)

(use-package caml-mode :straight t :defer t
  :bind (:map caml-mode-map
              ("C-c c" . nil)
              ("C-c t" . nil)
              ("C-c f" . nil)
              ("C-c m" . nil)))

(use-package org
  :init
  (defun /org-agenda-path (&optional subpath)
    (let ((subpath-actual (or subpath "")))
      (expand-file-name subpath-actual "~/org/agenda")))

  :custom
  (org-startup-indented t)
  (org-modules '(ol-w3m ol-bbdb ol-bibtex ol-docview ol-gnus ol-info
                        ol-irc ol-mhe ol-rmail ol-eww org-habit))
  (org-fold-core-style 'overlays)

  (org-agenda-files (mapcar '/org-agenda-path '("agenda.org" "project.org" "note.org"
                                                "birthday.org" "archive.org" "work.org")))
  (org-archive-location (/org-agenda-path "archive.org::"))
  (org-agenda-window-setup 'current-window)
  (org-agenda-include-diary t)
  (org-agenda-time-grid '((daily today require-timed)
                          (800 1000 1200 1400 1600 1800 2000)
                          "....." "----------------"))
  (org-agenda-current-time-string
   "now - - - - - - - - - - - - - - - - - - - - - - - - -")

  (org-log-done 'time)
  (org-clock-persist t)
  (org-read-date-display-live t)

  (org-capture-templates
   '(("t" "Task" entry (file+headline (/org-agenda-path "agenda.org") "Task") "* TODO %i%?")
     ("a" "Appointment" entry (file+headline (/org-agenda-path "agenda.org") "Inbox") "* %i%?")))

  :hook (org-agenda-mode-hook . (lambda () (cd (/org-agenda-path))))

  :bind (("C-c a c" . org-capture)
         ("C-c a d" . /org-rclone-sync-down)
         ("C-c a u" . /org-rclone-sync-up)
         ("C-c a a" . org-agenda)
         ("C-c a l" . org-store-link)
         ("C-c C-x <C-i>" . org-clock-in))

  :config
  (use-package org-habit)
  (use-package ox-latex
    :custom
    (org-latex-listings 'minted)
    (org-latex-src-block-backend 'minted)
    (org-latex-pdf-process ; `minted` requires shell escape when exporting
     '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
       "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
       "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
    :config
    (add-to-list 'org-latex-packages-alist '("" "minted")))

  (org-clock-persistence-insinuate)

  (add-to-list 'org-file-apps '("\\.x?html?" . "firefox %s"))
  (add-to-list 'org-file-apps '("\\.gif" . "mpv %s"))
  (add-to-list 'org-file-apps '("\\.mp4" . "mpv %s"))
  (add-to-list 'org-file-apps '("\\.mkv" . "mpv %s"))
  (add-to-list 'org-file-apps '("\\.wmv" . "mpv %s"))
  (add-to-list 'org-file-apps '("\\.m4v" . "mpv %s"))

  (defun /org-rclone-sync-down ()
    (interactive)
    (save-some-buffers)
    (shell-command
     (format "rclone sync openshell:/org %s && echo sync down successful" (/org-agenda-path))))

  (defun /org-rclone-sync-up ()
    (interactive)
    (save-some-buffers)
    (shell-command
     (format "rclone sync %s openshell:/org && echo sync up successful" (/org-agenda-path)))))


;;; Shell

(use-package shell
  :bind (:map shell-mode-map
              ;; Unset these keys so they don't overrride
              ;; my global bindings for resizing windows.
              ("<C-up>" . nil)
              ("<C-down>" . nil))
  :hook ((shell-mode-hook . ansi-color-for-comint-mode-on))
  :custom
  (shell-file-name (executable-find "bash"))
  (explicit-shell-file-name "/bin/bash")
  (shell-command-switch "-c")
  :config
  (use-package shx :straight t
    :custom (shx-path-to-convert "magick")))

(use-package bash-completion :straight t
  :ensure-system-package
  bash ("/usr/share/bash-completion/bash_completion" . bash-completion))

(use-package fish-completion :straight t
  :ensure-system-package fish
  :custom (fish-completion-fallback-on-bash-p t))

(use-package comint
  :custom (comint-terminfo-terminal "xterm-256color")
  :config
  (use-package xterm-color :straight t :demand t
    :custom
    (xterm-color-use-bold-for-bright t)
    (compilation-environment '("TERM=xterm-256color"))

    :hook ((shell-mode-hook . /xterm-color-shell-mode-setup)
           (comint-mode-hook . /xterm-color-shell-mode-setup))

    :config
    (defun /xterm-color-shell-mode-setup ()
      "Set up xterm-color for shell-mode"
      (font-lock-mode -1) ; Disable font-locking in this buffer to improve performance
      (make-local-variable 'font-lock-function) ; Prevent font-locking from being re-enabled in this buffer
      (setq font-lock-function (lambda (_) nil))
      (setq-local comint-output-filter-functions
                  (remove 'ansi-color-process-output comint-output-filter-functions))
      (add-hook 'comint-preoutput-filter-functions 'xterm-color-filter nil t))

    (defun /advice-compilation-filter (f proc string)
      "Render xterm colors in compile buffers"
      (funcall f proc (xterm-color-filter string)))
    (advice-add 'compilation-filter :around #'/advice-compilation-filter))

  (use-package coterm :straight t
    :bind (:map comint-mode-map ("C-;" . coterm-char-mode-cycle))))

(use-package vterm :straight t :defer t
  :custom
  (vterm-max-scrollback 100000)
  (vterm-kill-buffer-on-exit nil)
  (vterm-always-compile-module t) ;; Don't ask to compile module
  (vterm-copy-mode-remove-fake-newlines t)

  ;; Adding vterm prompt changes to my shell on remote hosts will break
  ;; tramp unless we account for it in the prompt pattern regexp.
  (tramp-shell-prompt-pattern "\\(?:^\\|\r\\)[^]#$%>\n]*#?[]#$%>].* *\\(^[\\[[0-9;]*[a-zA-Z] *\\)*")

  :bind (:map vterm-mode-map
              ("C-q" . vterm-send-next-key)
              ("<C-i>" . vterm-send-tab)
              ("C-<up>" . nil) ; Stop clash with my window resize keys
              ("C-<down>" . nil)
              ("C-c C-j" . vterm-copy-mode)
              :map vterm-copy-mode-map
              ("C-c C-k" . vterm-copy-mode))

  :config
  (add-to-list 'vterm-tramp-shells '("ssh" "/bin/bash")))

(use-package eshell :defer t
  :hook ((eshell-mode-hook . eshell-smart-initialize))
  :custom
  ;; Use the plan9 feature so I can read text from top to bottom
  (eshell-where-to-jump 'begin)
  (eshell-review-quick-commands nil)
  (eshell-smart-space-goes-to-end t)

  (eshell-cmpl-cycle-completions t)
  (eshell-cmpl-expand-before-complete t)
  (completions-format 'vertical)
  (eshell-cmpl-ignore-case t)


  (eshell-prompt /eshell-prompt)
  (eshell-prompt-regexp "^[^#$λ\n]*[#$λ] ")
  (eshell-highlight-prompt nil)

  :bind (:map eshell-mode-map
              ("C-c M-o" . /eshell-comint-clear))

  :config
  (use-package em-smart)

  (defun /eshell-prompt ()
    "My prompt for EShell"
    (concat
     (propertize (abbreviate-file-name (eshell/pwd)) 'face `(:foreground "#87af87"))
     (propertize (if (= (user-uid) 0) " #" " λ") 'face `(:foreground "#87af87"))
     (propertize (message " ") 'face `(:foreground "#D9D9D9"))))

  (defun /eshell-comint-clear ()
    "Clear `eshell' buffer, comint-style."
    (interactive)
    (let ((input (eshell-get-old-input)))
      (eshell/clear-scrollback)
      (eshell-emit-prompt)
      (insert input))))

(use-package term
  :custom (term-buffer-maximum-size 65536)
  :bind (:map term-mode-map
              ("C-y" . term-paste)))

(use-package exec-path-from-shell :straight t
  :config (exec-path-from-shell-initialize))

(use-package proced
  :custom (proced-enable-color-flag t))

(use-package tramp
  :custom
  (tramp-default-method "ssh")
  (tramp-default-remote-shell "/bin/bash")
  (password-cache t)
  (password-cache-expiry nil)
  (tramp-histfile-override nil))


;;; Display

(use-package page-break-lines :straight t :diminish)

(use-package paren
  :custom (show-paren-delay 0))

(use-package highlight-escape-sequences :straight t
  :hook (prog-mode-hook . hes-mode))

(use-package highlight-numbers :straight t
  :hook ((prog-mode-hook . highlight-numbers-mode)))

(use-package highlight-function-calls :straight t
  :hook ((prog-mode-hook . highlight-function-calls-mode)))

(use-package rainbow-delimiters :straight t
  :hook ((emacs-lisp-mode-hook . rainbow-delimiters-mode)))

(use-package fancy-battery :straight t
  :custom
  (fancy-battery-mode-line '(:eval (concat " " (fancy-battery-default-mode-line)))))

(use-package smart-mode-line :straight t
  :custom
  (sml/no-confirm-load-theme nil)
  (sml/show-remote t)
  (sml/modified-char "x")
  :config
  (setq sml/theme nil)) ; Use my own theme

(use-package whitespace
  :custom
  (whitespace-display-mappings '((space-mark   ?\     [?·]     [?.])
                                 (space-mark   ?\xA0  [?¤]     [?_])
                                 (newline-mark ?\n    [?¶ ?\n] [?$ ?\n])
                                 (tab-mark     ?\t    [?→ ?\t] [?\\ ?\t]))))


;;; Media

(use-package dired
  :custom
  (dired-dwim-target t)
  (dired-listing-switches
   (concat "-l --all --human-readable "
           "--group-directories-first --time-style=long-iso"))

  :hook ((dired-mode-hook . hl-line-mode))

  ;; Remove clashing bindings
  :bind (:map dired-mode-map
              ("M-b" . nil)
              ("C-S-b" . nil)
              ("C-<down>" . nil)
              ("C-<up>" . nil))

  :config
  (use-package dired-x
    :config
    (setq dired-omit-files (concat dired-omit-files "\\|^\\..+$")) ; omit dotfiles
    :hook ((dired-mode-hook . (lambda () (dired-omit-mode)))))

  (use-package dired-subtree :straight t
    :bind (:map dired-mode-map
                ("<tab>" . dired-subtree-toggle)
                ("<backtab>" . dired-subtree-cycle)))

  (use-package dired-collapse :straight t
    :bind (:map dired-mode-map
                ("C-c C-c" . dired-collapse-mode)))

  (use-package image-dired
    :custom
    (image-dired-thumb-width 400)
    (image-dired-cmd-create-thumbnail-options '("-i" "%f" "-s" "%w" "-c" "jpeg" "-o" "%t"))
    (image-dired-cmd-create-thumbnail-program "ffmpegthumbnailer"))

  ;; Use dired+ extensions
  (use-package dired+ :straight t
    :config
    (global-dired-hide-details-mode t)))

(use-package emms :straight t :defer t
  :ensure-system-package mpd mpc
  :custom
  (emms-browser-get-track-field-function 'emms-browser-get-track-field-albumartist)
  (emms-mode-line-icon-enabled-p nil)

  :bind (("<XF86AudioPlay>" . emms-pause)
         ("<XF86AudioStop>" . emms-stop)
         ("<XF86AudioPrev>" . emms-previous)
         ("<XF86AudioNext>" . emms-next)
         :map emms-browser-mode-map
         ("<tab>" . emms-browser-toggle-subitems)
         ("<backtab>" . emms-browser-toggle-subitems-recursively)
         ("SPC" . emms-browser-next-non-track)
         ("S-SPC" . emms-browser-prev-non-track))

  ;; Clean up emms directory. Its recreated and used in the correct place afterwards"
  :hook (after-init-hook . (lambda () (delete-directory (/user-path "emms") t)))

  :config
  (emms-all)

  (use-package emms-setup)
  (use-package emms-cue)
  (use-package emms-player-mpd
    :custom
    (emms-player-list '(emms-player-mpd))
    (emms-player-mpd-server-name "localhost")
    (emms-player-mpd-server-port "6600")
    (emms-player-mpd-music-directory "~/music")
    :config
    (add-to-list 'emms-info-functions 'emms-info-mpd)
    (shell-command "mpc consume off" nil)) ; `consume on` will break emms

  ;; Only show playing time on mode-line
  (emms-mode-line-disable)
  (emms-playing-time 1))

(use-package pdf-tools :straight t
  :custom (pdf-misc-print-program (executable-find "lpr"))
  :hook ((pdf-tools-enabled-hook . auto-revert-mode)) ; Handy for LaTeX coding
  :config
  (use-package pdf-view
    :custom
    (pdf-view-midnight-colors '("#D9D9D9" . "#1C1C1C"))
    (pdf-view-midnight-invert nil)
    (pdf-view-use-unicode-ligther nil)
    :hook ((pdf-view-mode-hook . pdf-view-midnight-minor-mode))
    :bind (:map pdf-view-mode-map
                ("M-s o" . pdf-occur)
                ("M-g M-g" . pdf-view-goto-page)))
  (use-package pdf-cache))

(use-package openwith :straight t
  :config
  (setq openwith-associations
        (list (list (openwith-make-extension-regexp
                     '("mpg" "mpeg" "mp3" "mp4" "m4v" "avi" "wmv" "wav"
                       "mov" "flv" "ogm" "ogg" "mkv" "webm" "m2ts"))
                    "mpv" '(file))
              (list (openwith-make-extension-regexp
                     '("doc" "xls" "ppt" "odt" "ods" "odg" "odp"))
                    "libreoffice" '(file)))))

(use-package transmission :straight t
  :ensure-system-package (transmission-daemon . transmission-cli)
  :hook ((transmission-mode-hook . hl-line-mode)))

(use-package elfeed :straight t :defer t
  :custom
  (elfeed-curl-max-connections 32)

  :config
  (defun /elfeed-search-open-link-mpv ()
    "Play the entry under cursor in elfeed-search-mode with mpv"
    (interactive)
    (elfeed-search-yank)
    (/mpv-kill-ring))

  (defun /elfeed-show-open-link-mpv ()
    "Play the current entry with mpv"
    (interactive)
    (/mpv (elfeed-entry-link elfeed-show-entry)))

  (defun /elfeed-podcast-tagger (entry)
    "Hook to tag podcast elfeed entries"
    (when (elfeed-entry-enclosures entry)
      (elfeed-tag entry 'pod)))

  (defun /elfeed-youtube-tagger (entry)
    "Hook to tag YouTube elfeed entries"
    (when (string-match "youtube.com" (elfeed-entry-link entry))
      (elfeed-tag entry 'yt)))

  :bind (("C-c z f" . elfeed)
         :map elfeed-search-mode-map
         ("o" . /elfeed-search-open-link-mpv)
         :map elfeed-show-mode-map
         ("o" . /elfeed-show-open-link-mpv))
  :hook ((elfeed-new-entry-hook . /elfeed-podcast-tagger)
         (elfeed-new-entry-hook . /elfeed-youtube-tagger)))

(use-package fortune-cookie :straight t
  :ensure-system-package (fortune . fortune-mod)
  :custom (fortune-cookie-fortune-command (executable-find "fortune")))

(use-package image+ :straight t
  :init (use-package image))

(use-package 0x0 :straight t)

(use-package ix :straight t)

(use-package browse-url
  :custom
  (browse-url-browser-function 'browse-url-firefox)
  (browse-url-firefox-new-window-is-tab t))

(use-package w3m :straight t :defer t
  :custom
  (w3m-display-mode 'plain) ; Use buffers instead of a tabs
  (w3m-use-header-line nil) ; Don't display header-line
  (w3m-fill-column 80)
  (w3m-key-binding 'info)
  (w3m-search-default-engine "duckduckgo")
  (w3m-show-graphic-icons-in-mode-line nil)
  :bind (:map w3m-mode-map
              ("C-l" . recenter-top-bottom)))

(use-package eww
  :custom
  (shr-width 70)
  (shr-use-colors nil)
  (shr-inhibit-images t)
  (eww-header-line-format nil)
  (eww-search-prefix "https://lite.duckduckgo.com/lite/?q="))

(use-package hackernews :straight t :defer t
  :bind (("C-c z h" . hackernews)))

(use-package cheat-sh :straight t :defer t)


;;; Communication

(use-package erc
  :custom
  (erc-fill-column 72)
  (erc-header-line-format nil)
  (erc-prompt (lambda () (concat (buffer-name) ">")))
  (erc-auto-query 'buffer) ; Open chats in a new buffer
  (erc-format-query-as-channel-p t)
  (erc-interpret-controls-p t) ; render control chars for color/formatting
  (erc-interpret-mirc-color t) ; full color support
  (erc-hide-list '("JOIN" "PART" "QUIT")) ; declutter channel messages

  ;; Turn on ERC tracking so that I can get see new mentions in my mode-line
  (erc-track-position-in-mode-line t)
  (erc-track-priority-faces-only 'all)
  (erc-track-faces-priority-list '(erc-current-nick-face))

  :hook ((erc-mode-hook . erc-track-mode)
         (erc-mode-hook . (lambda () (setq-local scroll-conservatively 101))))

  :config
  (use-package erc-hl-nicks :straight t
    :custom (erc-hl-nicks-skip-nicks '("so" "as" "nevermind")) ; common word names to ignore
    :hook ((erc-mode-hook . erc-hl-nicks-mode))))

;; Setting up my email accounts is done in .secret.el
(use-package mu4e :defer t
  :load-path "/usr/share/emacs/site-lisp/mu4e/"
  :ensure-system-package (mu . "yay -S mu") ;; Installed via system package `mu`
  :commands mu4e
  :custom
  (mail-user-agent 'mu4e-user-agent)
  (mu4e-maildir "~/mail/")
  (mu4e-get-mail-command "offlineimap")

  (mu4e-update-interval 600)
  (mu4e-index-update-in-background t)
  (mu4e-hide-index-messages t)

  (mu4e-headers-auto-update t)
  (mu4e-headers-date-format "%Y/%m/%d")
  (mu4e-headers-long-date-format "%Y/%m/%d")

  (mu4e-view-show-addresses t)
  (mu4e-context-policy 'pick-first)
  (mu4e-compose-signature-auto-include nil)
  (message-kill-buffer-on-exit t)

  :config
  (use-package smtpmail
    :custom
    (smtpmail-stream-type 'starttls)
    (smtpmail-smtp-service 587)
    (message-send-mail-function 'smtpmail-send-it)
    (smtpmail-servers-requiring-authorization ".*"))

  (use-package org-mime :straight t
    :hook ((message-send-hook . org-mime-confirm-when-no-multipart))))

(use-package ednc :straight t :diminish)

(use-package elcord :straight t :defer t
  :custom
  (elcord-use-major-mode-as-main-icon t)
  (elcord-display-elapsed nil)
  (elcord-refresh-rate 1)
  (elcord-show-small-icon nil)
  (elcord-editor-icon "emacs_legacy_icon"))

(use-package gptel :straight t :defer t
  :custom (gptel-default-mode 'org-mode)
  :bind (("C-c z g" . gptel-send)))
